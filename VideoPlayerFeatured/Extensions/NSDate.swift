//
//  NSDate.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 20/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public extension Date
{
    /**
     Return an instance of NSDate from a string formatted like yyyy-MM-dd
     */
    /*convenience init(dateString: String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        let d = dateStringFormatter.dateFromString(dateString)!
        self.init(timeInterval:0, sinceDate:d)
    }*/

    /**
     Return NSDate from a format like yyyy-MM-ddTHH:mm:ss.SSSSSSSZ with or without Milliseconds or UTC Z
     */
    /*convenience init(dateTimeString: String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")

        let isUTC = (dateTimeString.containsString("Z"))
        var dateTimeStringTmp = dateTimeString.stringByReplacingOccurrencesOfString("Z", withString: "")
        
        switch dateTimeStringTmp.characters.count {
        case 27:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
            break;
        case 26:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS"
            break;
        case 25:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSS"
            break;
        case 24:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS"
            break;
        case 23:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS"
            break;
        case 22:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            break;
        case 21:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
            break;
        case 20:
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
            break;
        case 10:
            dateStringFormatter.dateFormat = "yyyy-MM-dd"
            break;
        default:
            dateTimeStringTmp = (dateTimeStringTmp as NSString).substringToIndex(19)
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            break;
        }
        
        if (isUTC) {
            dateStringFormatter.dateFormat = dateStringFormatter.dateFormat + "'Z'"
            dateStringFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC")
        } else {
            dateStringFormatter.timeZone = NSTimeZone.init(abbreviation: "CET")
        }
        
        let d = dateStringFormatter.dateFromString(dateTimeString)!
        self.init(timeInterval:0, sinceDate:d)
    }*/
    
    /**
     Return an instance of NSDate from a string without format
     */
     init(dateTimeStringWithoutFormat: String) {
        let dateStringFormatter = DateFormatter()

        switch dateTimeStringWithoutFormat.characters.count {
        case 22:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSSSSSS"
            break;
        case 21:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSSSSS"
            break;
        case 20:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSSSS"
            break;
        case 19:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSSS"
            break;
        case 18:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSSS"
            break;
        case 17:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSSS"
            break;
        case 16:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssSS"
            break;
        case 15:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmssS"
            break;
        case 8:
            dateStringFormatter.dateFormat = "yyyyMMdd"
            break;
        default:
            dateStringFormatter.dateFormat = "yyyyMMddHHmmss"
            break;
        }
        
        dateStringFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateStringFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        let d = dateStringFormatter.date(from: dateTimeStringWithoutFormat)!
        self.init(timeInterval:0, since:d)
    }
}
