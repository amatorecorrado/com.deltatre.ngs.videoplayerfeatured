//
//  Extesions.swift
//  D3OTTCommon
//
//  Created by Corrado Amatore on 19/09/2017.
//
//

import Foundation

extension String{
    public func getSubstring(start: Int, end: Int) -> String{
        
        var substring: String = ""
        
        if(self.characters.count>0){
            let startIndex = self.index(self.startIndex, offsetBy: start)
            var endIndex = self.index(self.startIndex, offsetBy: end)
                
            let range = startIndex...endIndex
            substring = String(self[range])
        }
    
        return substring
    }
}


import AVFoundation


extension AVPlayerItem {
    
    enum TrackType {
        case subtitle
        case audio
        
        /**
         Return valid AVMediaSelectionGroup is item is available.
         */
        fileprivate func characteristic(item:AVPlayerItem) -> AVMediaSelectionGroup?  {
            let str = self == .subtitle ? AVMediaCharacteristic.legible : AVMediaCharacteristic.audible
            if item.asset.availableMediaCharacteristicsWithMediaSelectionOptions.contains(str) {
                return item.asset.mediaSelectionGroup(forMediaCharacteristic: str)
            }
            return nil
        }
    }
    
    func tracks(type:TrackType) -> [String] {
        if let characteristic = type.characteristic(item: self) {
            return characteristic.options.map { $0.displayName }
        }
        return [String]()
    }
    
    func selected(type:TrackType) -> String? {
        guard let group = type.characteristic(item: self) else {
            return nil
        }
        let selected = self.selectedMediaOption(in: group)
        return selected?.displayName
    }
    
    func select(type:TrackType, name:String) -> Bool {
        guard let group = type.characteristic(item: self) else {
            return false
        }
        guard let matched = group.options.filter({ $0.displayName == name }).first else {
            return false
        }
        self.select(matched, in: group)
        return true
    }
}
