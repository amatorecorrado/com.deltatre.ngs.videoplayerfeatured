//
//  ClipController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public class ClipController{
    private var _mainfeedUrl: String = ""
    private var _events: [Int: EventModel] = [:]
    private var _cameras: [CameraModel] = []
    public var isMatchEvent = true
    
    public init(){}
    
    public init(mainfeedUrl: String){
        _mainfeedUrl = mainfeedUrl
    }
    
    public init(mainfeedUrl: String, events: [Int:EventModel], cameras: [CameraModel] ){
        _mainfeedUrl = mainfeedUrl
        _events = events
        _cameras = cameras
    }
    
    public func get() -> ([ClipModel]) {
        
        var clips: [ClipModel] = []
        var clip:ClipModel = ClipModel()
        
        let feedNSUrl = NSURL(string: _mainfeedUrl)
        
        let jsonData = NSData(contentsOf: feedNSUrl! as URL)
        if (jsonData != nil && (jsonData?.length)! > 0) {
            do {
                if let json = try? JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments) as! [String: AnyObject]{
                    if let jsongroups = json["ClipGroups"] as? [Dictionary<String, AnyObject>]{
                        for clipGroups in jsongroups {
                            clip = ClipModel()
                            if let val = clipGroups["ID"] as? String {
                                clip.ID = val
                            }
                            
//                            if let val = clipGroups["timeCodeIn"] as? String {
//                                let dateFormatter = DateFormatter()
//                                dateFormatter.dateFormat = "yyyy-MM-ddThh:mm:ss.zzz"
//                                dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
//
//                                let dateObj = dateFormatter.date(from: val)
//
//                                clip.timeCodeIn = dateObj
//                            }
//                            if let val = clipGroups["ThumbnailUrl"] as? String {
//                                clip.ThumbnailUrl = val
//                            }
                            if let val = clipGroups["Url"] as? String {
                                clip.jsonDetailUrl = val
                            }
                            
                            if(clip.jsonDetailUrl != nil ){
                                getClipDetails(clip: &clip)
                                
                                if let min = clip.minute, min.count > 0, let type = clip.Event.eventType, clip.Videos.count > 0{
                                    if let eve = clip.eventID{
                                        print("Log Detail Clip: \(Int(eve))")
                                    }
                                    clips.append(clip)
                                }
                            }
                        }
                    }
                }
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        return clips
    }
    
    func getClipDetails( clip: inout ClipModel){
        let feedUrl = clip.jsonDetailUrl
        let feedNSUrl = NSURL(string: feedUrl!)
        
        var videos: [VideoClipModel] = []
        var video:VideoClipModel = VideoClipModel()
        
        let jsonData = NSData(contentsOf: feedNSUrl! as URL)
        if ((jsonData?.length)! > 0) {
            do {
                let json = try  JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments) as! [String: AnyObject]
                if let jsonclipDet = json["clipGroup"] as? [String: AnyObject] {
                    
                    if let val = jsonclipDet["Title"] as? String {
                        clip.Title = val
                    }
                    if let val = jsonclipDet["minute"] as? String, val.count > 0 {
                        clip.minute = val
                    }else{
                        return
                    }
                    
                    if let jsonbinding = jsonclipDet["Binding"] as? [String: AnyObject] {
                        
                        
                        if let val = jsonbinding["competitionID"] as? Int {
                            clip.competitionID = val
                        }
                        if let val = jsonbinding["competitionTypeID"] as? Int {
                            clip.competitionTypeID = val
                        }
                        if let val = jsonbinding["seasonID"] as? Int {
                            clip.seasonID = val
                        }
                        if let val = jsonbinding["roundID"] as? Int {
                            clip.roundID = val
                        }
                        if let val = jsonbinding["matchdayID"] as? Int {
                            clip.matchdayID = val
                        }
                        if let val = jsonbinding["seasonID"] as? Int {
                            clip.seasonID = val
                        }
                        if let val = jsonbinding["matchID"] as? Int {
                            clip.matchID = val
                        }
                        if let val = jsonbinding["eventID"] as? String {
                            clip.eventID = val
                            if(_events.count>0 && val.characters.count > 0){
                                clip.Event =  EventController.GetEvent(id: Int(val)!,events: _events)!
                            }
                        }
                    }
                    if let val = jsonclipDet["Phase"] as? String {
                        clip.Phase = val
                    }
                    for vids in (jsonclipDet["Videos"] as? [Dictionary<String, AnyObject>])! {
                        video = VideoClipModel()
                        
                        if let val = vids["Url"] as? String {
                            if val.hasSuffix(".m3u8"){
                                video.UrlHLS = val
                            }else if(val.hasSuffix(".mp4")){
                                video.UrlMP4 = val
                            }
                        }
                        
                        if let val = vids["ID"] as? String {
                            video.ID = val
                        }
                        if let val = vids["operation"] as? String {
                            video.operation = val
                        }
                        if let val = vids["version"] as? String {
                            video.version = val
                        }
                        if let val = vids["VideoFeed"] as? String {
                            video.VideoFeed = val
                            
                            video.Camera = CameraController.getCameraFromCode(code: val, cameras: _cameras)
                        }
                        if let val = vids["Target"] as? String {
                            video.Target = val
                        }
                        if let val = vids["Duration"] as? String {
                            video.Duration = val
                        }
                        if let val = vids["Codec"] as? String {
                            video.Codec = val
                        }
                        if let val = vids["Bitrate"] as? String {
                            video.Bitrate = val
                        }
                        if let val = vids["Language"] as? String {
                            video.Language = val
                        }
                        if let val = vids["ThumbnailUrl"] as? String {
                            video.ThumbnailUrl = val
                        }
                        
                        videos.append(video)
                    }
                    if let first = videos.first {
                        clip.ThumbnailUrl = first.ThumbnailUrl
                    }
                    clip.Videos = videos
                    
                }
                
            } catch {
                NSLog("error serializing JSON: \(error)")
            }
        }
        
    }
    public func getFromID(id: String, clips: [ClipModel]) -> ClipModel?{
        for clip in clips{
            if(clip.ID == id){
                return clip
            }
        }
        return nil
    }
    
    public func getClipVideoFromCameraID(cameraid: String, clips: [ClipModel])->VideoClipModel?
    {
        for clip in clips{
            for vid in clip.Videos{
                if vid.ID == cameraid{
                    return vid
                }
            }
        }
        return nil
    }
}
