//
//  CameraController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 19/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


public class CameraController{

    private var _feedUrl: String = ""
    
    public init(feedUrl: String){
        _feedUrl = feedUrl
    }
    public func get() -> [CameraModel]{
        var cameras: [CameraModel] = []
        let jsonCameras = NSData(contentsOf: NSURL(string: _feedUrl)! as URL)
        if ((jsonCameras?.length)! > 0) {
            do {
                let json = try JSONSerialization.jsonObject(with: jsonCameras! as Data, options: .allowFragments)  as? [String: AnyObject]
                for item in (json?["cameras"] as? [Dictionary<String, AnyObject>])!{
                    let ID = item["id"] as! String
                    let Name = item["name"] as! String
                    let Code = item["code"] as! String
                    cameras.append(CameraModel(ID: ID, Code: Code, Name: Name))
                }
                
            } catch {
                NSLog("error serializing JSON dictionary events type: \(error)")
            }
        }
        return cameras
    }
    
    public static func  getCameraFromCode(code: String, cameras: [CameraModel]) -> CameraModel?{
        for cam in cameras {
            if(cam.Code?.lowercased() ==  code.lowercased()){
                return cam
            }
        }
        var def = CameraModel();
        def.Code = code
        def.ID = code
        def.Name = code
        return def
    }
}
