//
//  Global.swift
//  D3OTTCommon
//
//  Created by Corrado Amatore on 27/09/2017.
//
//

import Foundation
	
public class Global
{
    public static var shared:Global = Global()
    
    public var isRefresh:Bool
    
    public init(){
        isRefresh = true
    }
}
