//
//  Match.swift
//  AppleTV
//
//  Created by Federico Bortoluzzi on 22/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public class VideoDataController: NSObject, XMLParserDelegate{

    private var videoData: VideoDataModel
    private var xmlParser: XMLParser!
    private var currentParsedElement = ""
    
    
    override public init(){
            //super.init()
            videoData = VideoDataModel()
    }
    var formatTemp = ""
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        self.currentParsedElement = elementName
        
        if (elementName == "event") {
            for (i,item) in attributeDict.enumerated() {
                if (String(item.key).lowercased() == "id") {
                    videoData.eventId = item.value
                }
            }
        } else if (elementName == "videoSource") {
            for (i,item) in attributeDict.enumerated() {
                if (String(item.key).lowercased() == "format") {

                    videoData.sources[item.value.lowercased()] = item.value.lowercased()
                    formatTemp = item.value.lowercased()

                }
            }
        }
    }
    
    public func parser(_ parser: XMLParser, foundCharacters string: String) {
        if (self.currentParsedElement == "uri") {
            videoData.sources[formatTemp] = string
        } else if (self.currentParsedElement == "timeCodeIn") {
            let dateTimeTmp = Date.init(dateTimeStringWithoutFormat: string)
            
            let dateStringFormatter = DateFormatter()
            dateStringFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateStringFormatter.locale = Locale(identifier: "en_US_POSIX") as Locale!
            dateStringFormatter.timeZone = TimeZone.init(abbreviation: "UTC")!
            
            videoData.timeCodeIn = dateStringFormatter.string(from: dateTimeTmp)
        }else if(self.currentParsedElement == "trimIn") {
            if(string.characters.count>0){
                var results:Double = 0
                
                
                let strings = string.components(separatedBy: ".")
                if let datetime = strings[0] as? String{
                    let datetimeArray = datetime.components(separatedBy: ":")
                    var hours = datetimeArray[0]
                    var minutes = datetimeArray[1]
                    var seconds = datetimeArray[2]
    //                var milli = string.getSubstring(start: 9,end: 11)

                    
                    
                    
                    if(hours.characters.count>0){
                        results = results + (3600 * Double(hours)! )
                    }
                    if(minutes.characters.count>0){
                        results = results + (60 * Double(minutes)! )
                    }
                    if(seconds.characters.count>0){
                        results = results + (Double(seconds)! )
                    }
    //                if(milli.characters.count>0){
    //                    results = results + Double(milli)!
    //                }
                }
                videoData.trimIn = results
            }
            
            
        }
    }
    
    public func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        self.currentParsedElement = ""
        videoData.format = ""
    }
    
    public func startParsing(data: NSData) {
        self.xmlParser = XMLParser(data: data as Data)
        self.xmlParser.delegate = self
        
        if (!self.xmlParser.parse()) {
            let error = self.xmlParser.parserError;
            NSLog("error parsing XML: \(error)")
        }
    }
    
//    public func getVideoData(feedVideoData: String,videoId: String) -> VideoDataModel {
//        
//
//            let xmlData = try NSData(contentsOf: NSURL(string: feedVideoData.replacingOccurrences(of:"{VIDEOID}", with: videoId))! as URL)
//            if ((xmlData?.length)! > 0) {
//                self.startParsing(data: xmlData!)
//            }
//            return videoData
//
//        
//    }
    
    public func getVideoData(feedVideoData: String,videoId: String) -> VideoDataModel? {
        
        do{
            let xmlData = try NSData(contentsOf: NSURL(string: feedVideoData.replacingOccurrences(of:"{VIDEOID}", with: videoId))! as URL)
            if (xmlData != nil && (xmlData?.length)! > 0) {
            self.startParsing(data: xmlData!)
            }
            videoData.videoId = videoId
            
            let tempHls = videoData.sources.filter({ (k,v) in
                return k == "hls"
            })
            
            let tempMp4 = videoData.sources.filter({ (k,v) in
                return k == "mp4"
            })

            if(tempHls.count>0){
                videoData.format = (tempHls.first?.key)!
                videoData.videoUrl = (tempHls.first?.value)!
            }else if(tempMp4.count>0){
                videoData.format = (tempMp4.first?.key)!
                videoData.videoUrl = (tempMp4.first?.value)!
            }
            
            
            return videoData
        }catch{
            
        }
        return nil

    }
}
