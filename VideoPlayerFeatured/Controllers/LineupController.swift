//
//  LineupController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


public struct LineupController {
    
    private var feedlineup: String
    private var playerThumbBaseUrl: String
    private var teamLogoBaseUrl: String
    
    public init(_feedlineupUrl: String,  _playerThumbBaseUrl: String, _teamLogoBaseUrl: String){
        feedlineup = _feedlineupUrl
        playerThumbBaseUrl = _playerThumbBaseUrl
        teamLogoBaseUrl = _teamLogoBaseUrl
    }
    
    public func get(matchId: Int, events: [EventModel]) -> (LineupModel) {
        var results = LineupModel()
        var homes: [PlayerModel] = []
        var aways: [PlayerModel] = []
        
        var player = PlayerModel()
        let matchIdStr = String(matchId)
        let feedUrl = feedlineup
        
        if let encodedString = feedUrl.addingPercentEncoding(
            withAllowedCharacters: NSCharacterSet.urlFragmentAllowed),
            let feedNSUrl = NSURL(string: encodedString) {
            
            
            let jsonData = NSData(contentsOf: feedNSUrl as URL)
            if ((jsonData?.length)! > 0) {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments) as! [String:AnyObject]
                    
                    //GET PLAYERS
                    if let modules = json["modules"] as? [String: AnyObject] {
                        if let jsonLineups = modules["lineups"] as? [String: AnyObject] {
                            if let homeTeam = jsonLineups["home"] as? [String: AnyObject] {
                                
                                player = createTeam(teamObjJson: homeTeam)
                                homes.append(player)
                                
                                for homeTeamPitch in (homeTeam["pitch"] as? [Dictionary<String, AnyObject>])! {
                                    player = createPlayer(playerObjJson: homeTeamPitch, type: "lineup")
                                    homes.append(player)
                                }
                                for homeTeamPitch in (homeTeam["bench"] as? [Dictionary<String, AnyObject>])! {
                                    player = createPlayer(playerObjJson: homeTeamPitch, type: "bench")
                                    homes.append(player)
                                }
                                
                            }
                            
                            if let awayTeam = jsonLineups["away"] as? [String: AnyObject] {
                                
                                player = createTeam(teamObjJson: awayTeam)
                                aways.append(player)
                                
                                for awayTeamPitch in (awayTeam["pitch"] as? [Dictionary<String, AnyObject>])! {
                                    
                                    player = createPlayer(playerObjJson: awayTeamPitch, type: "lineup")
                                    aways.append(player)
                                }
                                for awayTeamPitch in (awayTeam["bench"] as? [Dictionary<String, AnyObject>])! {
                                    
                                    player = createPlayer(playerObjJson: awayTeamPitch, type: "bench")
                                    aways.append(player)
                                }
                            }
                        }
                    }
                    
                    results.Homes = homes
                    results.Aways = aways
                    
                    //                //GET EVENTS
                    //                var eventObj: EventModel
                    //                var eventArray: [EventModel]
                    //
                    //                if let jsonPlayers = json["eventsforplayer"] as? [String: AnyObject] {
                    //                    for jplayer in jsonPlayers{
                    //
                    //                        eventArray = []
                    //
                    //                        let playerID = Int(jplayer.0)
                    //                        let playerObj = getPlayer(playerID: playerID!, lineups: results)
                    //
                    //                        let jevent = jplayer.1
                    //                        let jevents = jevent as! [[String : AnyObject]]
                    //
                    //                        for eve in jevents{
                    //                            eventObj = EventModel()
                    //
                    //                            if let val = eve["id"] as? Int {
                    //                                eventObj.id = val
                    //                            }
                    //                            if let val = eve["descr"] as? String {
                    //                                eventObj.descr = val
                    //                            }
                    //                            if let val = eve["code"] as? Int {
                    //                                eventObj.code = val
                    //                            }
                    //                            if let val = eve["minute"] as? Int {
                    //                                eventObj.minute = val
                    //                            }
                    //                            if let val = eve["playerFrom"] as? Int {
                    //                                eventObj.playerFrom = val
                    //                            }
                    //                            if let val = eve["playerTo"] as? Int {
                    //                                eventObj.playerTo = val
                    //                            }
                    //                            if let val = eve["subCode"] as? Int {
                    //                                eventObj.subCode = val
                    //                            }
                    //                            if let val = eve["type"] as? Int {
                    //                                eventObj.type = val
                    //                            }
                    //                            if let val = eve["tag"] as? String {
                    //                                eventObj.tag = val
                    //                            }
                    //                            if let val = eve["time"] as? String {
                    //
                    ////                                let dateString = "Thu, 22 Oct 2015 07:45:17 +0000"
                    ////                                dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
                    //
                    //
                    //                                //let dateString = "2017-08-22T20:06:00.59"
                    //                                let dateFormatter = DateFormatter()
                    //                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    //                                //dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
                    //
                    //                                let index = val.index(val.startIndex, offsetBy: 19)
                    //                                let subs = val.substring(to: index)
                    //
                    //                                let dateObj = dateFormatter.date(from: subs)
                    //
                    //                                eventObj.time = dateObj! as NSDate
                    //
                    //
                    //                            }
                    //                            if let val = eve["teamTo"] as? Int {
                    //                                eventObj.teamTo = val
                    //                            }
                    //                            if let val = eve["teamFrom"]! as? Int {
                    //                                eventObj.teamFrom = val
                    //                            }
                    //                            if let val = eve["phase"] as? Int {
                    //                                eventObj.phase = val
                    //                            }
                    //                            if let val = eve["isValidated"] as? Bool {
                    //                                eventObj.isValidated = val
                    //                            }
                    //                            if let val = eve["injuryMinute"] as? Int {
                    //                                eventObj.injuryMinute = val
                    //                            }
                    //                            eventArray.append(eventObj)
                    //                        }
                    //                        playerObj?.events = eventArray
                    //                    }
                    //                }
                    
                } catch {
                    NSLog("error serializing JSON: \(error)")
                }
            }
        }
        
        return results
    }
    
    func getPlayer(playerID: Int, lineups: LineupModel) -> PlayerModel?{
        for h_player in lineups.Homes! {
            if(h_player.ID == playerID){
                return h_player
            }
        }
        for a_player in lineups.Aways! {
            if(a_player.ID == playerID){
                return a_player
            }
        }
        return nil
    }
    
    func createPlayer(playerObjJson: Dictionary<String, AnyObject>, type: String) -> PlayerModel{
        let player = PlayerModel()
        
        player.ID = playerObjJson["id"] as? Int
        player.BibNumber = playerObjJson["jerseyNumber"] as? String
        player.OfficialName = playerObjJson["officialName"] as? String
        player.OfficialSurname = playerObjJson["officialSurname"] as? String
        player.ShortName = playerObjJson["shortName"] as? String
        player.Role = playerObjJson["role"] as? String
        player.Type = type
        
        //player.ShortName = playerObjJson["ShortName"] as? String
        player.IsCaptain = playerObjJson["isCaptain"] as! Bool
        player.IsGoalkeeper = (player.Role == "goalkeepers")
        
        if let thumb = playerObjJson["photo"] as? String {
            player.ThumbSmall = thumb
            player.ThumbMedium = thumb
            player.ThumbLarge = thumb
        }
        
        //player.PhotoURL = self.playerThumbBaseUrl.stringByReplacingOccurrencesOfString("{PLAYERID}", withString: "\(player.ID!)")
        
        return player
    }
    
    func createTeam(teamObjJson: Dictionary<String, AnyObject>) -> PlayerModel{
        let team = PlayerModel()
        team.ID = teamObjJson["id"] as? Int
        team.OfficialSurname = teamObjJson["name"] as? String
        team.ThumbLarge = teamObjJson["logo"] as? String
        team.ThumbMedium = teamObjJson["logo"] as? String
        team.ThumbSmall = teamObjJson["logo"] as? String
        team.IsTeam = true
        return team
    }
}
