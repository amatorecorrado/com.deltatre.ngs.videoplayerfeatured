//
//  translations.swift
//  AppleTV
//
//  Created by administrator on 30/06/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
//import EVReflection


public struct Player {
    var name: String
    var surname: String
    var webname: String
    
    public init()
    {
        self.name = ""
        self.surname = ""
        self.webname = ""
        
    }
    
    static public func deserialize(items: [String : AnyObject]) -> [String: Player] {
        var playerOBJ = Player();
        var result: [String: Player] = [:]
    
        
        for item in items {
            //var tmpPlayer = Player()
            
            let tmpId = item.0
            let tmpPlayer = item.1 as? [String: String]
            
            if let val = tmpPlayer!["name"] {
                playerOBJ.name = val
            }
            if let val = tmpPlayer!["surname"] {
                playerOBJ.surname = val
            }
            if let val = tmpPlayer!["webname"] {
                playerOBJ.webname = val
            }
            
            result[tmpId] = playerOBJ
        }
        return result
    }
}


public struct Team {
    var name: String
    var officialname: String

    public init()
    {
        self.name = ""
        self.officialname = ""
    }
    
    static public func deserialize(items: [String : AnyObject]) -> [String: Team] {
        var teamOBJ = Team();
        var result: [String: Team] = [:]
        
        
        for item in items {
            let tmpId = item.0
            let tmpTeam = item.1 as? [String: String]
            
            if let val = tmpTeam!["name"] {
                teamOBJ.name = val
            }
            if let val = tmpTeam!["officialname"] {
                teamOBJ.officialname = val
            }
            
            result[tmpId] = teamOBJ
        }
        return result
    }
}

public struct Referee {
    var name: String
    var surname: String
    var webname: String
    var role: String
    
    public init()
    {
        self.name = ""
        self.surname = ""
        self.webname = ""
        self.role = ""
    }
    
    static public func deserialize(items: [String : AnyObject]) -> [String: Referee]  {
        var refereeOBJ = Referee();
        var result: [String: Referee] = [:]
        
        for item in items {
            let tmpId = item.0
            let tmpReferee = item.1 as? [String: String]
            
            if let val = tmpReferee!["name"] {
                refereeOBJ.name = val
            }
            if let val = tmpReferee!["surname"] {
                refereeOBJ.surname = val
            }
            if let val = tmpReferee!["webname"] {
                refereeOBJ.webname = val
            }
            if let val = tmpReferee!["role"] {
                refereeOBJ.role = val
            }
            
            result[tmpId] = refereeOBJ
        }
        return result
    }

}

public class Translations {
    public var players: Dictionary<String, Player> = [:]
    public var rounds: Dictionary<String, String> = [:]
    public var groups: Dictionary<String, String> = [:]
    public var generictags: Dictionary<String, String> = [:]
    public var countries: Dictionary<String, String> = [:]
    public var venues: Dictionary<String, String> = [:]
    public var teams: Dictionary<String, Team> = [:]
    public var staffs: Dictionary<String, Player> = [:]
    public var referees: Dictionary<String, Referee> = [:]
    public var stadiums: Dictionary<String, String> = [:]
    public var apptranslations: Dictionary<String, String> = [:]
    
    public init()
    {
        
    }
    
    public func getTranslations(urlFeedTranslations : String) -> Translations {
        let results: Translations = Translations()
        var jsonurltranslation = NSURL()

        jsonurltranslation = NSURL(string: urlFeedTranslations)!
        let jsondata = NSData(contentsOf: jsonurltranslation as URL)
        let dataString = String(data: jsondata! as Data, encoding: String.Encoding.utf8)
        let data = dataString!.data(using: String.Encoding.utf8, allowLossyConversion: false)!

        if (jsondata!.length > 0) {
    
        do {

            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: AnyObject]
            
            let tmpTrans = json["translations"] as! [String: AnyObject]
            
            let tmpPlay = tmpTrans["players"] as! [String: AnyObject]
            results.players = Player.deserialize(items: tmpPlay)
            
            let tmpStaffs = tmpTrans["staffs"] as! [String: AnyObject]
            results.staffs = Player.deserialize(items: tmpStaffs)
            

            let tmpTeams = tmpTrans["teams"] as! [String: AnyObject]
            results.teams = Team.deserialize(items: tmpTeams)

            
            let tmpReferees = tmpTrans["referees"] as! [String: AnyObject]
            results.referees = Referee.deserialize(items: tmpReferees)

            
            let tmpRounds = tmpTrans["rounds"] as! [String: String]
            for item in tmpRounds {
                let tmpId = item.0
                let tmpValue = item.1
                results.rounds[tmpId] = tmpValue
            }

            let tmpGroups = tmpTrans["groups"] as! [String: String]
            for item in tmpGroups {
                let tmpId = item.0
                let tmpValue = item.1
                results.groups[tmpId] = tmpValue
            }

            let tmpGenerictags = tmpTrans["generictags"] as! [String: String]
            for item in tmpGenerictags {
                let tmpId = item.0
                let tmpValue = item.1
                results.generictags[tmpId] = tmpValue
            }

            let tmpCountries = tmpTrans["countries"] as! [String: String]
            for item in tmpCountries {
                let tmpId = item.0
                let tmpValue = item.1
                results.countries[tmpId] = tmpValue
            }

            let tmpVenues = tmpTrans["venues"] as! [String: String]
            for item in tmpVenues {
                let tmpId = item.0
                let tmpValue = item.1
                results.venues[tmpId] = tmpValue
            }
            
            let tmpStadiums = tmpTrans["stadiums"] as! [String: String]
            for item in tmpStadiums {
                let tmpId = item.0
                let tmpValue = item.1
                results.stadiums[tmpId] = tmpValue
            }
            
            let tmpAapptranslations = tmpTrans["apptranslations"] as! [String: String]
            for item in tmpAapptranslations {
                let tmpId = item.0
                let tmpValue = item.1
                results.apptranslations[tmpId] = tmpValue
            }
            
            
        }
         catch {
            NSLog("error serializing JSON: \(error)")
            }
            
 }
      return results
}
}

