//
//  Notification.swift
//  AppleTV
//
//  Created by Corrado Amatore on 27/06/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct EventController
{
    private var _urlFeedEvents = ""
    
    public init (urlFeedEvents: String){
        _urlFeedEvents = urlFeedEvents
    }
    
    
    public func getNotifications(parameters: ParametersModel) -> ([EventModel]) {
        
        var results: [EventModel] = []
        
        if let cupcode = parameters.CupCode, let season = parameters.Season, let matchid = parameters.MatchId{
            
            
            var notification: EventModel
            var jsonurl = NSURL()
            
            if let url: String? = Utility.ReplaceParameters(text: _urlFeedEvents, parameters: parameters)
            {
                jsonurl = NSURL(string: url!)!
            }
            let jsonData = NSData(contentsOf: jsonurl as URL)
            if ((jsonData?.length)! > 0) {
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments) as! [String: AnyObject]
                    if let modules = json["modules"] as? [String: AnyObject] {
                        for notifications in (modules["events"] as? [Dictionary<String, AnyObject>])! {
                            var timeSet:Bool = false
                            
                            notification = EventModel()
                            if let val = notifications["eventId"] as? Int {
                                notification.id = val
                            }
                            if let val = notifications["attributes"] as? String {
                                notification.attributes = val.components(separatedBy: ",")
                            }
                            if let val = notifications["minute"] as? Int {
                                notification.minute = val
                            }
                            if let val = notifications["playerFromId"] as? Int {
                                notification.playerFromId = val
                            }
                            if let val = notifications["playerToId"] as? Int {
                                notification.playerToId = val
                            }
                            if let val = notifications["playerFromName"] as? Int {
                                notification.playerFromName = val
                            }
                            if let val = notifications["playerToName"] as? Int {
                                notification.playerToName = val
                            }
                            if let val = notifications["type"] as? String {
                                notification.eventType = val
                            }
                            if let val = notifications["timeUTC"] as? String, val.count > 18 {
                                let dateFormatter = DateFormatter()
                                let prefix = val.prefix(19)
                                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                //dateFormatter.locale = Locale.init(identifier: "en_US_POSIX")
//                                let index = val.index(val.startIndex, offsetBy: 19)
//                                let subs = val.substring(to: index)
//                                let dateObj = dateFormatter.date(from: subs)
                                if (prefix.count == 19){
                                    let dateObj = dateFormatter.date(from: String(prefix))
                                    notification.timeUTC = dateObj! as NSDate
                                    timeSet = true;
                                }
                            }
                            
                            if let val = notifications["teamFromId"] as? Int {
                                notification.teamFromId = val
                            }
                            
                            if let val = notifications["teamToId"] as? Int {
                                notification.teamToId = val
                            }
                            
                            if let val = notifications["teamToName"] as? String {
                                notification.teamToName = val
                            }
                            if let val = notifications["teamFromName"]! as? String {
                                notification.teamFromName = val
                            }
                            
                            if let val = notifications["phaseId"] as? Int {
                                notification.phaseId = val
                            }
                            
                            if let val = notifications["description"] as? String {
                                notification.description = val
                            }
                            //notification.code != nil && notification.code! > 0 &&
                            if notification.id != nil && notification.id! != 0 &&  notification.description != nil  && (notification.description?.characters.count)! > 0 && timeSet {
                                results.append(notification)
                            }
                        }
                    }
                    
                } catch {
                    NSLog("error serializing JSON: \(error)")
                }
            }
        }
        return results
    }
    
    public static func GetEvent(id: Int, events: [Int: EventModel])->EventModel?{
        
        if let event = events[id]{
            return event
        }
        return EventModel()
    }
    
    public static func GetEventById(id: Int, events: [EventModel])->EventModel?{
        for event in events{
            if(event.id == id){
                return event
            }
        }
        return EventModel()
    }
    
    
}

