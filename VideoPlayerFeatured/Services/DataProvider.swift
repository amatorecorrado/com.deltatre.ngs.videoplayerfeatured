//
//  DataProvider.swift
//  AppleTV
//
//  Created by Corrado Amatore on 03/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


class DataProvider{
    
    var matchTranslation: [String:Translations]
    //var notificationList: [EventModel] = []
    
    var oldNotifications: [EventModel]
    private var _newNotifications: [EventModel]
    var newEventsDictionary:[Int: EventModel] = [:]
    var newNotifications: [EventModel]{
        set{
            _newNotifications = newValue
            playerEvents = [:]
            newValue.forEach{event in
                if let eventid = event.id{
                    newEventsDictionary[event.id!] = event
                }
                if let playerId = event.playerFromId {
                    if var events = playerEvents[playerId]{
                        events.append(event)
                        playerEvents[playerId] = events
                    }else{
                        var events: [EventModel] = []
                        events.append(event)
                        playerEvents[playerId] = events
                    }
                    
                }
                if event.eventType?.lowercased() == "substitution", let playerId = event.playerToId {
                    if var events = playerEvents[playerId]{
                        events.append(event)
                        playerEvents[playerId] = events
                    }else{
                        var events: [EventModel] = []
                        events.append(event)
                        playerEvents[playerId] = events
                    }
                    
                }
            }
        }
        get{
            return _newNotifications
        }
    }
    
    var playerEvents: [Int:[EventModel]] = [:]
    
    var oldLineups: LineupModel
    private var _newLineups: LineupModel
    var newLineups: LineupModel{
        set{
            newValue.Aways?.forEach{player in
                if let playerId = player.ID{
                    player.events = playerEvents[playerId]
                }
            }
            newValue.Homes?.forEach{player in
                if let playerId = player.ID{
                    player.events = playerEvents[playerId]
                }
            }
            _newLineups = newValue
        }
        get{
            return _newLineups
        }
    }
    var oldSchedules: Schedule
    var newSchedules: Schedule
    
    var oldClips: [ClipModel]
    var newClips: [ClipModel]
    
    var newMatch: Match?
    var oldMatch: Match?
    
    var matchNotifications: [Int:[EventModel]]
    var matchClips: [Int:[ClipModel]]
    var matchLineups: [Int:LineupModel]
    var schedules: [String:Schedule]
    
    var _cameraTraslation: [CameraModel]
    var cameraTraslation: [CameraModel]{
        get{
            if(self._cameraTraslation.count == 0){
                let controller = CameraController(feedUrl: feedCameraTraslation)
                self._cameraTraslation = controller.get()
            }
            return self._cameraTraslation
        }
        set(value){
            self._cameraTraslation = value
        }
    }
    
    var eventsIconsCommentaryBaseUrl: String = ""
    var feedVideodataUrl: String = ""
    var feedEventsNGS: String = ""
    var feedClips: String = ""
    var feedCameraTraslation: String = ""
    var feedLineups: String = ""
    var feedMatchdaySchedule = ""
    //var feedTranslations = ""
    var feedMatch = ""
    var playerThumbBaseUrl: String = ""
    var teamLogoBaseUrl: String = ""
    //var isLive: Bool = false
    
    var repeatingTimer = RepeatTimer()
    //var fetchInterval: Int = 0
    
    static let sharedInstance = DataProvider()
    
    init(){
        
        matchTranslation = [:]
        matchNotifications = [:]
        matchClips = [:]
        matchLineups = [:]
        schedules = [:]
        
        _cameraTraslation = []
        oldNotifications = [EventModel]()
        _newNotifications = [EventModel]()
        
        oldLineups = LineupModel()
        _newLineups = LineupModel()
        
        oldSchedules = Schedule()
        newSchedules = Schedule()
        
        oldClips = []
        newClips = []
        
    }
    
    func setParameters(configuration: ConfigurationModel){
        
        //FEEDS
        if let data = configuration.feeds.getValue(key: "feedVideodata"){
            feedVideodataUrl = data
        }
        if let data = configuration.feeds.getValue(key: "feedEventsNGS"){
            feedEventsNGS = data
        }
        if let data = configuration.feeds.getValue(key: "feedClips"){
            feedClips = data
        }
        if let data = configuration.feeds.getValue(key: "feedVocabularyCamera"){
            feedCameraTraslation = data
        }
        if let data = configuration.feeds.getValue(key: "feedLineups"){
            feedLineups = data
        }
        if let data = configuration.feeds.getValue(key: "feedMatchdaySchedule"){
            feedMatchdaySchedule = data
        }
        if let data = configuration.feeds.getValue(key: "feedMatch"){
            feedMatch = data
        }

        //IMAGES
        if let url = configuration.images.getValue(key: "playerThumb"){
            playerThumbBaseUrl = url
        }
        if let url = configuration.images.getValue(key: "events_icons_commentary"){
            eventsIconsCommentaryBaseUrl = url
        }
        if let url = configuration.images.getValue(key: "teamLogo"){
            teamLogoBaseUrl = url
        }
        
    }
    
    func fetchMatchData(parameters: ParametersModel, fetchInterval: Int = 0){

        DispatchQueue.global().async(execute: {
            self.getMatch(parameters: parameters)
            self.getNotifications(parameters: parameters)
            self.getLineups(parameters: parameters)
            self.getSchedule(parameters: parameters)
            self.getClips(parameters: parameters)
            
            if(fetchInterval>0){
                
                DispatchQueue.main.sync{
                    self.repeatingTimer.intervalSeconds = fetchInterval
                    self.repeatingTimer.timer.setEventHandler(handler: { [weak self] in
                        // called every so often by the interval we defined above
                        self?.fetchMatchData(parameters: parameters)
                    })
                    self.repeatingTimer.resume()
                }
            }
            
        })
    }
    
    func dismissMatchData(){
        repeatingTimer.deinitTimer()
        
        oldNotifications = [EventModel]()
        newNotifications = [EventModel]()
        
        oldLineups = LineupModel()
        newLineups = LineupModel()
        
        oldSchedules = Schedule()
        newSchedules = Schedule()
        
        oldClips = []
        newClips = []
        
        newMatch = nil
        oldMatch = nil
        
    }
    
    
    // ----- DATA GATHERING
    
    func getEventsDictionary(parameters: ParametersModel) -> [Int:EventModel]{
        if(newEventsDictionary.count > 0){
            return newEventsDictionary
        }else{
            self.getNotifications(parameters: parameters)
            return newEventsDictionary
        }
    }
    
    func getMatchNotifications(parameters: ParametersModel) -> [EventModel]{
        if(newNotifications.count > 0){
            return newNotifications
        }else{
            self.getNotifications(parameters: parameters)
            return newNotifications
        }
    }
    
    func getMatchLineups(parameters: ParametersModel) -> LineupModel{
        if(newLineups.Homes != nil && newLineups.Aways != nil && newLineups.Homes!.count > 0 && newLineups.Aways!.count > 0){
            return newLineups
        }else{
            return self.getLineups(parameters: parameters)
        }
    }
    
    func getMatchClips(parameters: ParametersModel) -> [ClipModel]{
        if(newClips.count > 0){
            return newClips
        }else{
            return self.getClips(parameters: parameters)
        }
    }
    
    func getMatches(parameters: ParametersModel, actualDate: NSDate) -> Schedule{
        var _sched: Schedule?
        if newSchedules.Matches.count > 0 {
            _sched = newSchedules
        }else{
            _sched =  self.getSchedule(parameters: parameters)
        }
        
        let datematchstr: String = Utility.setDateValue(date: actualDate)
        
        _sched!.Matches = self.filterMatchesByDate(date: datematchstr, matches: _sched!.Matches)
        
        return _sched!
    }
    
    func getMatchData(parameters: ParametersModel) -> Match?{
        if(newMatch != nil){
            return newMatch
        }else{
            return self.getMatch(parameters: parameters)
        }
    }
    
//    func getMatchTranslation(cup: Int, season: Int) -> Translations{
//        let key: String = String(cup) + "_" + String(season)
//        if(matchTranslation[key] == nil){
//            matchTranslation[key] = getTranslation(cup: cup, season: season)
//        }
//
//        return matchTranslation[key]!
//    }
    
    func filterMatchesByDate(date: String, matches: [Match]) -> [Match]{
        var res: [Match] = []
        
        for match: Match in matches
        {
            
            let userCalendar = Calendar(identifier: .gregorian)
            let hourMinuteComponents = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second])
            let datetime = userCalendar.dateComponents(
                hourMinuteComponents,
                from: match.DateCET as Date)
            
            let datematchstr: String = String(describing: datetime.year)+String(format: "%02d", datetime.month!)+String(format: "%02d", datetime.day!)
            if(date == datematchstr){
                res.append(match)
            }
        }
        return res
    }
    
    
    //MARK:-
    //MARK: DATA CALLS
    
    
    private func getNotifications(parameters: ParametersModel) -> [EventModel]{
        var _notificationList: [EventModel] = []
        let eventController = EventController(urlFeedEvents: self.feedEventsNGS)
        _notificationList = eventController.getNotifications(parameters: parameters)
        matchNotifications[Int(parameters.MatchId!)!] = _notificationList
        
        
        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.notifications")
        lockQueue.sync() {
            oldNotifications = newNotifications
            newNotifications = _notificationList.reversed()
        }
        NotificationCenter.default.post(NSNotification(name: NSNotification.Name(rawValue: "notificationsDataReady"), object: nil) as Notification)
        print(" --- > Notifications matches data updated, match id \(parameters.MatchId), found items: \(newNotifications.count)")
        return newNotifications
    }
    
    private func getClips(parameters: ParametersModel) -> [ClipModel]{
        
        var _clipList:[ClipModel] = []
        if let cupcode = parameters.CupCode, let matchId = parameters.MatchId{
            self.feedClips = Utility.ReplaceParameters(text: self.feedClips, parameters: parameters)
            let clipController = ClipController(mainfeedUrl: self.feedClips, events: self.getEventsDictionary(parameters: parameters), cameras: self.cameraTraslation)
        _clipList = clipController.get()
        matchClips[Int(matchId)!] = _clipList
        
        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.clips")
        lockQueue.sync() {
            oldClips    = newClips
            newClips = _clipList
        }
        
        NotificationCenter.default.post(NSNotification(name: NSNotification.Name(rawValue: "multicamDataReady"), object: nil) as Notification)
        print(" --- > Clips data updated, match id \(matchId), found items: \(newClips.count)")
        }
        return _clipList
        
    }
    
    private func getLineups(parameters: ParametersModel) -> LineupModel{
        var lineupsObj = LineupModel()
        
        if let cup = parameters.Cup,let season = parameters.Season, let matchId = parameters.MatchId{
            let playerPhotoURL = self.playerThumbBaseUrl.replacingOccurrences(of:"{CUP}", with: cup).replacingOccurrences(of:"{SEASON}", with: season)
            self.feedLineups = Utility.ReplaceParameters(text: self.feedLineups, parameters: parameters)
            let controller = LineupController(_feedlineupUrl: self.feedLineups,  _playerThumbBaseUrl: playerPhotoURL,_teamLogoBaseUrl: self.teamLogoBaseUrl)
            
            lineupsObj = controller.get(matchId: Int(matchId)!, events: self.newNotifications)
            matchLineups[Int(matchId)!] = lineupsObj
            
            let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.lineups")
            lockQueue.sync() {
                oldLineups = newLineups
                newLineups = lineupsObj
            }
            
            NotificationCenter.default.post(NSNotification(name: NSNotification.Name(rawValue: "lineupsDataReady"), object: nil) as Notification)
            print(" --- > Lineups data updated, match id \(matchId)")
        }
        return lineupsObj
    }
    
    
    func getSchedule(parameters: ParametersModel) -> Schedule{
        
        var schedule = Schedule()
        
        if let cup = parameters.Cup,let season = parameters.Season, let matchday = parameters.Matchday, let roundId = parameters.RoundId{
            
                let matchDayScheduleUrl = URL(string: Utility.ReplaceParameters(text: self.feedMatchdaySchedule, parameters: parameters))
            
            let jsonData = NSData(contentsOf: matchDayScheduleUrl!)
                if ((jsonData?.length)! > 0) {
                    do {
                        let json = try JSONSerialization.jsonObject(with: jsonData! as Data, options: .allowFragments)
                        schedule = Schedule.deserialize(json: (json as? Dictionary<String, AnyObject>)!, feedMatch: feedMatch)
                        
                        let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.schedule")
                        lockQueue.sync() {
                            oldSchedules = newSchedules
                            newSchedules = schedule
                        }
                        
                        let key: String = cup + "_" + season + "_" + roundId + "_" + matchday
                        schedules[key] = schedule
                        
                    }
                    catch {
                        NSLog("error serializing JSON: \(error)")
                    }
                    
                }
            
                NotificationCenter.default.post(NSNotification(name: NSNotification.Name(rawValue: "matchesDataReady"), object: nil) as Notification)
                print(" --- > Schedule matches data updated, match day \(matchday), found items: \(newSchedules.Matches.count)")
        }
        return schedule
    }
    
    func getMatch(parameters: ParametersModel) -> Match? {
        
        var match: Match?
        
        
        if let cupCode = parameters.CupCode,let season = parameters.Season, let matchid = parameters.MatchId{
            
            self.feedMatch = Utility.ReplaceParameters(text: self.feedMatch, parameters: parameters)
            let matchUrl = NSURL(string:  self.feedMatch)!
            do {
                //let jsonString = try NSData(contentsOf: matchUrl as URL)
                
                //jsonString = jsonString.replacingOccurrences(of:"{\"matchbox\":", with: "")//.replacingOccurrences(of:"}]}}", with: "}]}")
                
                //jsonString = jsonString.substringToIndex(jsonString.endIndex.advancedBy(-1))
                //jsonString = "{" + jsonString + "}"
                let jsonData = NSData(contentsOf: matchUrl as URL)
                //let jsonData = jsonString.data(using: String.Encoding.utf8)
                
                if let jsonData = jsonData, jsonData.length > 0 {
                    
                    let json = try JSONSerialization.jsonObject(with: jsonData as Data, options: .allowFragments)  as? [String: AnyObject]
                    if let modules = json?["modules"] as? [String: AnyObject]{
                        var comp: [String: AnyObject] = [:]
                        var mInfo: [String: AnyObject] = [:]
                        if let competitions = modules["competition"] as? Array<AnyObject>, let competition = competitions.first as? [String:AnyObject]{
                            comp = competition
                        }
                        if let matchInfo = modules["matchInfo"] as? [String: AnyObject]{
                            mInfo = matchInfo
                        }
                        match = Match.deserialize(competition: comp, matchInfo: mInfo)
                        if let videos = mInfo["videos"] as? Array<AnyObject>{
                            match!.Videos = MatchVideos.deserialize(videos: videos)
                            let lockQueue = DispatchQueue(label: "com.deltatre.lockqueue.match")
                            lockQueue.sync() {
                                oldMatch = newMatch
                                newMatch = match
                            }
                        }
                        
                    }
                    
                }
                
            }catch {
                NSLog("error serializing JSON: \(error)")
            }
            NotificationCenter.default.post(NSNotification(name: NSNotification.Name(rawValue: "matchDataReady"), object: nil) as Notification)
            print(" --- > Match data updated, match id \(matchid), found ")
        }
        return match
    }
    
    
    
//    func getTranslation(cup: Int, season: Int) -> Translations
//    {
//        if let url: String? = self.feedTranslations.replacingOccurrences(of:"{CUPID}", with: String(cup)).replacingOccurrences(of:"{SEASONID}", with: String(season)){
//            var translationData = Translations()
//            translationData = translationData.getTranslations(urlFeedTranslations: url!)
//            let key: String = String(cup) + "_" + String(season)
//            matchTranslation[key] = translationData
//            return translationData
//
//        }
//    }
    
    
}
