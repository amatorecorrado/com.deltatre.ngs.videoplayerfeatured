//
//  MAC.swift
//  CryptoSwift
//
//  Created by Marcin Krzyzanowski on 03/09/14.
//  Copyright (c) 2014 Marcin Krzyzanowski. All rights reserved.
//

import CryptoSwift
/**
*  Message Authentication
*/
public enum Authenticator {
    
    public enum AuthError: Error {
        case AuthenticateError
    }
    
    /**
    Poly1305
    
    - parameter key: 256-bit key
    */
    case Poly1305(key: Array<UInt8>)
    case HMAC(key: Array<UInt8>, variant:CryptoSwift.HMAC.Variant)
    
    /**
    Generates an authenticator for message using a one-time key and returns the 16-byte result
    
    - returns: 16-byte message authentication code
    */
    public func authenticate(message: Array<UInt8>) throws -> Array<UInt8> {
        switch (self) {
        case .Poly1305(let key):
            do{
            let auth = try CryptoSwift.Poly1305(key: key).authenticate(message)
                return auth
                
            }catch{
                    throw AuthError.AuthenticateError
                }
            
        case .HMAC(let key, let variant):
            do{
            let auth = try CryptoSwift.HMAC(key: key, variant: variant).authenticate(message)
                return auth
                
            }catch{
                throw AuthError.AuthenticateError
            }
            
        }
        throw AuthError.AuthenticateError
    }
}
