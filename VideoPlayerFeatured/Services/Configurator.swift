//
//  Configurator.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

//private var _sharedInstanceConfigurator: ConfiguratorController!

public class Configurator{
    public var navigationController: UINavigationController?
    //var videoPlayer: ViewPlayer?
    //var videoPlayerSimple: ViewPlayerSimple?
        var alert = UIAlertController()
    
    private init() { }
    // MARK: Shared Instance
    
    public static let shared = Configurator()
    
//    class var sharedInstance: ConfiguratorController {
//
//        struct Static {
//            static var onceToken: dispatch_once_t = 0
//            static var instance: ConfiguratorController? = nil
//        }
//        dispatch_once(&Static.onceToken) {
//            Static.instance = ConfiguratorController()
//        }
//        return Static.instance!
//    }
//    public static func sharedInstance() -> ConfiguratorController{
//        return .shared
//    }
    public func  action(url: String, additionalData: AnyObject?){

            let config = read(configUrl: url)
            if let config = config, let type = config.type , type.characters.count > 0 {
                singletonSetup(config: config)
                goToView(config: config,additionalData: additionalData)
            }else{
                let alert = UIAlertController.init(title: "Error loading", message: "Configuration view missing", preferredStyle: UIAlertControllerStyle.alert)
                self.navigationController?.pushViewController(alert, animated: true)
            }
        
        
    }
    
    public func  actionVideoPlayer(config: ConfigurationModel, additionalData: AnyObject?) -> ViewPlayer?{
        if let type = config.type , type.characters.count > 0 {
            singletonSetup(config: config)
            var dict = additionalData as? [String: String]
            
            if let dict = dict{
                Global.shared.isRefresh = false
                let controller = ViewPlayer()
                if let videourl = dict["videoUrl"], let matchIdString = dict["matchId"] , let matchId = Int(matchIdString), let videoId = dict["currentVideoId"]{
                    
                    
                    controller.videoUrl = videourl
                    controller.matchId = matchId
                    controller.currentVideoId = videoId
                    controller.viewConfiguration = config
                    
                    return controller
                }
                //self.navigationController!.pushViewController(controller, animated: true)
            }
            return nil
            
        }else{
            let alert = UIAlertController.init(title: "Error loading", message: "Configuration view missing", preferredStyle: UIAlertControllerStyle.alert)
            self.navigationController?.pushViewController(alert, animated: true)
        }
        
        return nil
    }
    
    public func  actionVideoPlayer(url: String, additionalData: AnyObject?) -> ViewPlayer?{
        
        let config = read(configUrl: url)
        if let config = config{
            return actionVideoPlayer(config: config, additionalData: additionalData)
        }
        return nil
    }
    
    public func  actionVideoPlayerSimple(url: String, additionalData: AnyObject?) -> ViewPlayerSimple?{
        
        
        let config = read(configUrl: url)
        if(config != nil && config?.type != nil && (config?.type?.characters.count)! > 0){
            singletonSetup(config: config!)
            var dict = additionalData as? [String: String]
            
            if(dict != nil){
                Global.shared.isRefresh = false
                var videoPlayerSimple = ViewPlayerSimple()
                videoPlayerSimple.VideoUrl = dict!["videoUrl"]!
                videoPlayerSimple.viewConfiguration = config
                
                return videoPlayerSimple
                
            }
            return nil
            
        }else{
            let alert = UIAlertController.init(title: "Error loading", message: "Configuration view missing", preferredStyle: UIAlertControllerStyle.alert)
            self.navigationController?.pushViewController(alert, animated: true)
        }
        
        return nil
    }
    
    func goToView(config: ConfigurationModel, additionalData: AnyObject?){
        switch config.type! {
//        case "menu":
//            let controller = ViewHome()
//            
//            NSLog("Dispatcher View --> Go to View Home")
//            controller.viewConfiguration = config
//            self.navigationController!.pushViewController(controller, animated: false)
//            break
//        case "live":
//            let controller = LiveViewController(nibName: "LiveViewController", bundle: nil)
//            NSLog("Dispatcher View --> Go to View Live")
//            controller.viewConfiguration = config
//            self.navigationController!.pushViewController(controller, animated: true)
//            break
//        case "matches":
//            let controller = MatchesViewController(nibName: "MatchesViewController", bundle: nil)
//            NSLog("Dispatcher View --> Go to View Matches")
//            controller.viewConfiguration = config
//            self.navigationController!.pushViewController(controller, animated: true)
//            break
//        case "highlights":
//            let controller = ViewHighlights()
//            //viewHighlights.videos =  videosHighlights
//            
//            NSLog("Dispatcher View --> Go to View Highlights")
//            controller.viewConfiguration = config
//            self.navigationController!.pushViewController(controller, animated: true)
//            break
//        case "backgroundview":
//            let viewBackground = ViewBackground()
//            viewBackground.viewConfiguration = config
//            NSLog("Dispatcher View --> Go to View Background")
//            self.navigationController!.pushViewController(viewBackground, animated: true)
//            break
        case "player":
            NSLog("Dispatcher View --> Go to View Player")
            
            var dict = additionalData as? [String: String]
            
            if(dict != nil){
                let controller = ViewPlayer()
                controller.videoUrl = dict!["videoUrl"]!
                controller.matchId = Int(dict!["matchId"]!)!
                controller.currentVideoId = dict!["currentVideoId"]!
               
                controller.viewConfiguration = config
                
                self.navigationController!.pushViewController(controller, animated: true)
            }
            break
//        case "player_simple":
//            NSLog("Dispatcher View --> Go to View Player Simple")
//            if(videoPlayerSimple != nil){
//                videoPlayerSimple!.viewConfiguration = config
//                self.navigationController!.pushViewController(videoPlayerSimple!, animated: true)
//            }
//            break
            
        default:
            
            break
        }
        
    }
    
    func read(configUrl: String) -> ConfigurationModel?{
        let configurator = ConfigurationModel()
        let url = NSURL(string: configUrl)!
        do {
        let nsdata =  try Data(contentsOf: url as URL)
            if ((nsdata.count) > 0) {            
                let json = try JSONSerialization.jsonObject(with: nsdata as Data, options: .allowFragments) as? [String: AnyObject]
                
                if let val = json?["id"] as? String {
                    configurator.id = val
                }
                if let val = json?["type"] as? String {
                    configurator.type = val
                }
                if let val =  json?["parameters"]{
                    if(val != nil){
                        for param in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: param, dict: &(configurator.parameters))
                        }
                    }
                }
                if let val =  json?["feeds"]{
                    if(val != nil){
                        for feed in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: feed, dict: &(configurator.feeds))
                        }
                    }
                }
                if let val =  json?["images"]{
                    if(val != nil){
                        for image in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: image, dict: &(configurator.images))
                        }
                    }
                }
                if let val =  json?["actions"]{
                    if(val != nil){
                        for action in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: action, dict: &(configurator.actions))
                        }
                    }
                }
                if let val =  json?["labels"]{
                    if(val != nil){
                        for label in (val as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: label, dict: &(configurator.labels))
                        }}
                }
                if let val =  json?["data"]{
                    if(val != nil){
                        for data in (json?["data"] as? [Dictionary<String, AnyObject>])! {
                            addValueToDictionary(value: data, dict: &(configurator.data))
                        }
                    }
                }
            
        }
        } catch {
            NSLog("error serializing JSON: \(error)")
        }
        return configurator
    }
    
    func singletonSetup(config: ConfigurationModel){
        let actualVersion = config.parameters["imagesVersion"]
        if (actualVersion != nil &&  actualVersion != UserDefaults.standard.string(forKey: "imagesVersion")){
            ImagesUtility.sharedInstance.forceDownload = true
            ImagesUtility.sharedInstance.useLocalStorage = true
            //result.Images = ImagesUtility(forceDownload: true, useLocalStorage: true)
            UserDefaults.standard.set(actualVersion, forKey: "imagesVersion")
        }else{
            ImagesUtility.sharedInstance.forceDownload = false
            ImagesUtility.sharedInstance.useLocalStorage = true
            //ImagesUtility.setup(false, useLocalStorage: true)
            //result.Images = ImagesUtility(forceDownload: false, useLocalStorage: true)
        }
    }
    
    func addValueToDictionary(value: [String:AnyObject], dict: inout [String:String]){
        if let name = value["name"] as? String {
            if let val = value["value"] as? String {
                dict[name] = val
            }
        }
    }

    
    
}
