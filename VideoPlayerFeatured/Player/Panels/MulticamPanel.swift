//
//  MulticamPanel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 07/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

protocol MulticamPanelDelegate{
    func multicamSelected(cameras: ClipModel)
    func multiangleSelected(cameraid: String)
    func goBack()
}

class MulticamPanel: BaseViewController{
    
    var delegate: MulticamPanelDelegate?
    
    let multiangleCameraScrollView = AdvancedScrollview()
    var clipController: ClipController = ClipController()
    var clipList: [ClipModel] = []
    let screenRect = UIScreen.main.bounds
    
    var counterPenalty = 1
    var multicamItemWidth: CGFloat = 370
    var camerasItemWidth: CGFloat = 370
    
    let spinnerMulticam = UIActivityIndicatorView(activityIndicatorStyle: .white)
    let warningLabel = UILabel()
    var panelMultiAngleMenu = UIView()
    let multiangleCameraHighlightedScrollView = UIScrollView()
    let multiangleCameraSelectedLabel = UILabel()
    let multiangleCameraNameSelectedLabel = UILabel()
    let multiangleCameraSelectedScrollView = UIScrollView()
    var timerMultiangle: Timer? = Timer.init()
    var viewMultiangleCameraIsActive: Bool = false
    var viewMultiangleCameraHighlightedIsActive: Bool = false
    
    
    var selectedPlayByPlay: String = ""
    var isLive: Bool = false
    var cup:Int = 0
    var season:Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    var matchId: Int = 0
    var indexMulticam = 1
    var urlEventsIcons = ""
    var skin: String = ""
    
    var parameters = ParametersModel()
    
    public var warnLabel: String = "Multicam not available for this match."
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    init(parameters: ParametersModel, isLive: Bool, urlEventsIcons: String, skin: String, warning: String){
        super.init(nibName: nil, bundle: nil)
        self.parameters = parameters
        self.cup = Int(parameters.Cup!)!
        self.season = Int(parameters.Season!)!
        self.roundId = Int(parameters.RoundId!)!
        self.matchDay = Int(parameters.Matchday!)!
        self.matchId = Int(parameters.MatchId!)!
        self.isLive = isLive
        self.urlEventsIcons = urlEventsIcons
        self.skin = skin
        
        if(warning.characters.count>0){
            self.warnLabel = warning
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
        self.spinnerMulticam.frame = CGRect(x: self.screenRect.width / 2 - 20, y: 900, width: 40, height: 40)
        
        DispatchQueue.main.async {
            self.view.addSubview(self.spinnerMulticam)
            self.spinnerMulticam.startAnimating()
        }
        
        multiangleMenuCreation(panel: &panelMultiAngleMenu)
        self.view.addSubview(panelMultiAngleMenu)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MulticamPanel.handleMulticam(notification:)), name: NSNotification.Name(rawValue: "multicamDataReady"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.clipList = DataProvider.sharedInstance.getMatchClips(parameters: parameters)
        
        //translationData = DataProvider.sharedInstance.getTranslation(cup, season: season)
        DispatchQueue.main.async() {
            self.showMulticameraCamera()
        }
        
        viewMultiangleCameraIsActive = true
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        NSLog("Dismiss NotificationPartialView animated")
        if(viewMultiangleCameraIsActive){
            
            Utility.timerStop(timer: &timerMultiangle)
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.panelMultiAngleShow()
            }, completion: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "multicamDataReady"), object: nil)
        
        
    }
    
    func showMulticameraCamera() {
        counterPenalty = 1
        //let imagesWidth = CGFloat(integerLiteral: 295)
        let imagesHeight = CGFloat(integerLiteral: 165)
        
        for item in multiangleCameraScrollView.subviews {
            item.removeFromSuperview()
        }
        
        if (clipList.count > 0) {
            
            var i = 0, y = 0
            for item:ClipModel in self.clipList {
                drawMulticam(item: item,isAdd: false, isUpdate: false, view: UIView())
                y += 1
                i += 1
            }
            
            self.multiangleCameraScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            
            multiangleCameraScrollView.frame = CGRect(x: 50, y: 820, width: screenRect.width - 100, height: 300)
            let calculateWidth = CGFloat(integerLiteral: multiangleCameraScrollView.subviews.count) * multicamItemWidth
            multiangleCameraScrollView.contentSize = CGSize(width: calculateWidth, height: 300)
            multiangleCameraScrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
            multiangleCameraScrollView.isScrollEnabled = true
            multiangleCameraScrollView.clipsToBounds = false
            
            self.view.addSubview(multiangleCameraScrollView)
            
            var k = 0
            for item in multiangleCameraScrollView.subviews {
                UIView.animate(withDuration: 0.5, delay: Double(Double(k * 1) / 10), options: UIViewAnimationOptions.curveEaseOut, animations: {
                    item.frame.origin.y = 0
                }, completion: nil)
                k += 1
                
                
            }
            
            if (spinnerMulticam.isAnimating) {
                spinnerMulticam.stopAnimating()
            }
            
            
        }else{
            warningLabel.frame = CGRect(x: self.screenRect.width / 2 - 400, y: 770 + (imagesHeight/2) + 20, width: 800, height: 40)
            warningLabel.font = UIFont.init(name: "Dosis-Regular", size: 28)
            warningLabel.textAlignment = .center
            warningLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            
            warningLabel.text = warnLabel.uppercased()
            
            self.view.addSubview(warningLabel)
            
            if (spinnerMulticam.isAnimating) {
                spinnerMulticam.stopAnimating()
            }
            
            return
        }
    }
    
    func drawMulticam(item: ClipModel, isAdd: Bool, isUpdate: Bool, view: UIView){
        
        let imagesWidth = CGFloat(integerLiteral: 295)
        let imagesHeight = CGFloat(integerLiteral: 165)
        let offset = CGFloat(integerLiteral: multiangleCameraScrollView.subviews.count) * (imagesWidth+75)
        
        var buttonFrame = CGRect(x: offset, y: 1000, width: imagesWidth, height: imagesHeight + 100)
        let multiNumberFrame = CGRect(x: 275, y: -15, width: 40, height: 40)
        let multiNumberLabelFrame = CGRect(x: 275, y: -15, width: 40, height: 40)
        let imagesFrame = CGRect(x: 0, y: 0, width: imagesWidth, height: imagesHeight)
        let iconFrame = CGRect(x: 0, y: 205, width: 50, height: 36)
        let separatorFrame = CGRect(x: 48, y: 203, width: 1, height: 35)
        let timeFrame = CGRect(x: 50, y: 195, width: 70, height: 50)
        let eventFrame = CGRect(x: 119, y: 195, width: 176, height: 50)
        
        
        let img = Utility.GetCachedImage(key: "multicam_thumbs_" + item.eventID!, url: item.ThumbnailUrl!)
        
        if(img != nil){
            let videosCount:Int = item.Videos.count
            
            if (isUpdate || isAdd) {
                buttonFrame.origin.y = 0
                buttonFrame.origin.x = view.frame.origin.x
            }
            
            let button = AdvancedButton(type: UIButtonType.custom)
            button.tag = indexMulticam
            button.IDString = item.ID!
            button.frame = buttonFrame
            button.adjustsImageWhenHighlighted = true
            button.addTarget(self, action: #selector(MulticamPanel.handleMultiangleCameraButton(sender:)), for: UIControlEvents.allEvents)
            
            let multiNumberView = UIImageView(image: UIImage(named: "MultiNumber"))
            multiNumberView.frame = multiNumberFrame
            
            let multiNumberLabel = UILabel(frame: multiNumberLabelFrame)
            multiNumberLabel.text = String(videosCount)
            multiNumberLabel.font = UIFont.init(name: "Dosis-Regular", size: 20.0)
            multiNumberLabel.textAlignment = NSTextAlignment.center
            multiNumberLabel.textColor = UIColor.white
            
            let imgView = UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
            imgView.frame = imagesFrame
            //imgView.adjustsImageWhenAncestorFocused = true
            imgView.contentMode = .scaleAspectFill
            imgView.addSubview(multiNumberView)
            imgView.addSubview(multiNumberLabel)
            button.addSubview(imgView)
            
            if let eventType = item.Event.eventType{
                
                let urlIcon: String = urlEventsIcons + eventType + ".png"
                
                if(urlIcon.characters.count>0){
                    
                    let img = Utility.GetCachedImage(key: "multicam_events_" + eventType, url: urlIcon)
                    if(img != nil){
                        //let iconViewImg = UIImageView(frame: iconFrame)
                        let imgView = UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: 50, height: 36), withAplha: 1))
                        imgView.frame = iconFrame
                        button.addSubview(imgView)
                    }
                    
                }
                
            }
            
            let separatorViewImg = UIImageView(image: UIImage(named: "Separator"))
            separatorViewImg.frame = separatorFrame
            button.addSubview(separatorViewImg)
            
            let gameTime = item.minute!
            let gameTimeSplitted = gameTime.components(separatedBy: "+")
            
            let timeLabel = UILabel(frame: timeFrame)
            
            //PENALTIES
            if(item.Event.phaseId == 5)
            {
                timeLabel.text = "P." + String(counterPenalty)
                counterPenalty += 1
            }else{
                timeLabel.text = gameTimeSplitted[0]
            }
            
            timeLabel.font = UIFont.init(name: "Dosis-Regular", size: 47.0)
            timeLabel.textAlignment = NSTextAlignment.center
            timeLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            timeLabel.lineBreakMode = NSLineBreakMode.byClipping
            button.addSubview(timeLabel)
            
            let eventLabel = UILabel(frame: eventFrame)
            eventLabel.text = String("\(item.Title!)").uppercased()
            eventLabel.font = UIFont.init(name: "Dosis-Regular", size: 18)
            eventLabel.textAlignment = NSTextAlignment.left
            eventLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            eventLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            eventLabel.numberOfLines = 0
            button.addSubview(eventLabel)
            
            indexMulticam += 1
            
            if (isUpdate) {
                multiangleCameraScrollView.insertSubview(button, belowSubview: view)
            } else {
                multiangleCameraScrollView.addSubview(button)
            }
        }
    }
    
    func panelMultiAngleRemoveDelayed(){
        Utility.timerStop(timer: &timerMultiangle)
        timerMultiangle = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.panelMultiAngleRemove), userInfo: nil, repeats: false)
    }
    
    @objc func panelMultiAngleRemove(){
        self.panelMultiAngleMenu.alpha = 0
        multiangleCameraNameSelectedLabel.alpha = 0
        multiangleCameraSelectedLabel.alpha = 0
        multiangleCameraSelectedScrollView.alpha = 0
    }
    
    func panelMultiAngleShow(){
        self.panelMultiAngleMenu.alpha = 1
        multiangleCameraNameSelectedLabel.alpha = 1
        multiangleCameraSelectedLabel.alpha = 1
        multiangleCameraSelectedScrollView.alpha = 1
        self.panelMultiAngleMenu.layer.zPosition = 1000
    }
    
    
    
    func showMulticameraCameraHighlighted() {
        
        for item in multiangleCameraSelectedScrollView.subviews {
            item.removeFromSuperview()
        }
        
        if (clipList.count > 0) {
            let playByPlay = clipController.getFromID(id: selectedPlayByPlay, clips: clipList)
            if(playByPlay != nil){
                multiangleCameraSelectedLabel.text = playByPlay?.Title!.uppercased()
                let cameraname: String = (playByPlay?.Videos[0].Camera!.Name!.uppercased())!
                multiangleCameraNameSelectedLabel.text = cameraname
                
                var i = 0;
                for item:VideoClipModel in playByPlay!.Videos {
                    drawCamera(item: item, isFirst: i==0, isAdd: false, isUpdate: false, view: UIView())
                    i += 1;
                }
                //DELEGATO
                self.delegate?.multicamSelected(cameras: playByPlay!)
            }
            self.multiangleCameraSelectedScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            multiangleCameraSelectedScrollView.frame = CGRect(x: 50, y: 820, width: screenRect.width - 100, height: 300)
            let calculateWidth = CGFloat(integerLiteral: multiangleCameraSelectedScrollView.subviews.count) * self.multicamItemWidth
            multiangleCameraSelectedScrollView.contentSize = CGSize(width: calculateWidth + 50, height: 300)
            multiangleCameraSelectedScrollView.layoutMargins = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            multiangleCameraSelectedScrollView.autoresizingMask = UIViewAutoresizing.flexibleWidth
            multiangleCameraSelectedScrollView.isScrollEnabled = true
            multiangleCameraSelectedScrollView.clipsToBounds = false
            multiangleCameraSelectedScrollView.layer.zPosition = 1000
            
            
            self.view.addSubview(multiangleCameraSelectedScrollView)
            self.view.superview?.bringSubview(toFront: self.view)
            
            multiangleCameraHighlightedScrollView.isHidden = false
            multiangleCameraSelectedScrollView.isHidden = false
            
            self.panelMultiAngleShow()
            
            panelMultiAngleRemoveDelayed()
            
            multiangleCameraScrollView.isHidden = true
            
            var k = 0
            for item in multiangleCameraSelectedScrollView.subviews {
                UIView.animate(withDuration: 0.5, delay: Double(Double(k * 1) / 10), options: UIViewAnimationOptions.curveEaseOut, animations: {
                    item.frame.origin.y = 30
                }, completion: nil)
                k += 1
            }
            
            if (spinnerMulticam.isAnimating) {
                spinnerMulticam.stopAnimating()
            }
        }
    }
    
    func drawCamera(item: VideoClipModel, isFirst: Bool, isAdd: Bool, isUpdate: Bool, view: UIView) {
        let imagesWidth = CGFloat(integerLiteral: 255)
        let imagesHeight = CGFloat(integerLiteral: 135)
        
        let offset = CGFloat(integerLiteral: multiangleCameraSelectedScrollView.subviews.count) * (imagesWidth + 75)
        
        var buttonFrame = CGRect(x: offset, y: 1000, width: imagesWidth, height: imagesHeight + 100)
        let imagesFrame = CGRect(x: 0, y: 0, width: imagesWidth, height: imagesHeight)
        let cameraFrame = CGRect(x: 0, y: 150, width: imagesWidth, height: 30)
        let iconFrame = CGRect(x: 120, y: 190, width: 20, height: 20)
        if (isUpdate || isAdd) {
            buttonFrame.origin.y = 0
            buttonFrame.origin.x = view.frame.origin.x
        }
        
        let button = AdvancedButton()
        button.IDString = item.ID!
        button.frame = buttonFrame
        button.adjustsImageWhenHighlighted = true
        button.addTarget(self, action: #selector(MulticamPanel.handleMultiangleCameraHighlightedButton(sender:)), for: UIControlEvents.allEvents)
        
        
        
        let imgUrl = NSURL(string: item.ThumbnailUrl!)
        let imgData = NSData(contentsOf: imgUrl! as URL)
        let img = UIImage(data: imgData! as Data)
        
        let imgView = UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
        imgView.frame = imagesFrame
        //imgView.adjustsImageWhenAncestorFocused = true
        imgView.contentMode = .scaleAspectFill
        button.addSubview(imgView)
        
        let cameraLabel = UILabel(frame: cameraFrame)
        var cameraname: String = ""
        if(item.Camera != nil){
            cameraname  = item.Camera!.Name!
        }else{
            cameraname  = item.VideoFeed!
        }
        cameraLabel.text =  cameraname //String("\(item.CameraName!)").replacingOccurrences(of:"_", with: " ").uppercaseString
        cameraLabel.font = UIFont.init(name: "Dosis-Regular", size: 25)
        cameraLabel.textAlignment = NSTextAlignment.center
        cameraLabel.textColor = UIColor.white
        button.addSubview(cameraLabel)
        
        let iconView = UIImageView(image: UIImage(named: "Dot"))
        iconView.frame = iconFrame
        iconView.tag = 2000
        if (!isFirst) {
            iconView.isHidden = true
        }
        //button.addSubview(iconView)
        
        if (isUpdate) {
            multiangleCameraSelectedScrollView.insertSubview(button, belowSubview: view)
        } else {
            multiangleCameraSelectedScrollView.addSubview(button)
        }
        //multiangleCameraSelectedStackView.addArrangedSubview(button)
    }
    
    @objc func handleMultiangleCameraButton(sender: AdvancedButton!) {
        NSLog("MultiangleCameraButton tag --> " + String(sender.tag))
        
        let eventid = sender.IDString
        selectedPlayByPlay = eventid
        
        self.showMulticameraCameraHighlighted()
        viewMultiangleCameraHighlightedIsActive = true
        
        //_ = Timer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(), userInfo: nil, repeats: false)
    }
    
    
    @objc func handleMultiangleCameraHighlightedButton(sender: AdvancedButton!) {
        NSLog("MultiangleCameraHighlightedButton tag --> " + String(sender.tag))
        
        let cameraid = sender.IDString
        
        let videoclip = clipController.getClipVideoFromCameraID(cameraid: cameraid, clips: clipList)
        
        var cameraname: String = ""
        if(videoclip!.Camera != nil){
            cameraname  = videoclip!.Camera!.Name!
        }else{
            cameraname  = videoclip!.VideoFeed!
        }
        multiangleCameraNameSelectedLabel.text = cameraname
        
        self.delegate?.multiangleSelected(cameraid: cameraid)
        
        self.view.superview?.bringSubview(toFront: self.view)
        self.panelMultiAngleMenu.layer.zPosition = 1000
    }
    
    func multiangleMenuCreation( panel: inout UIView){
        
        let arrowRight = UIImageView(image: UIImage(named: "ArrowRight"))
        arrowRight.frame = CGRect(x: screenRect.size.width - 180,  y: screenRect.size.height / 2 - 50, width: 100, height: 100)
        //arrowRight.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
        arrowRight.alpha = 0.7
        arrowRight.tag = 102
        
        let arrowLeft = UIImageView(image: UIImage(named: "ArrowLeft"))
        arrowLeft.frame = CGRect(x: 80, y: screenRect.size.height / 2 - 50, width: 100,height: 100)
        //arrowLeft.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        arrowLeft.alpha = 0.7
        arrowLeft.tag = 104
        panel.alpha = 0
        //panel.frame = CGRect(x: 0, y: 780, width: 1920, height: 300)
        //        panel.addSubview(arrowRight)
        //        panel.addSubview(arrowLeft)
        
        //        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.ExtraLight)
        //        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //        blurEffectView.frame = CGRect(x: 0, y: 817, width: 1920, height: 250)
        panel.frame = CGRect(x: 0, y: 817, width: 1920, height: 300)
        panel.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        //panel.addSubview(blurEffectView)
        
    }
    
    func updateMulticam(){
        //---- REFRESH NOTIFICATIONS AND MULTICAM ------
        let newclips = DataProvider.sharedInstance.newClips
        let oldclips = DataProvider.sharedInstance.oldClips
        
        if(oldclips.count>0){
            var previousMulticamView = UIView()
            
            
            
            if  newclips.count > 0, oldclips.count>0 {
                
                for item in newclips {
                    var isPresent = false
                    
                    for itemOld in oldclips {
                        if (item.ID == itemOld.ID) {
                            isPresent = true
                            if let eventType = item.Event.eventType, let oldEventType = itemOld.Event.eventType{
                                if (item.eventID != itemOld.eventID ||
                                    item.Title != itemOld.Title ||
                                    item.minute != itemOld.minute ||
                                    eventType != oldEventType ||
                                    //item.Event.attributes! != itemOld.Event.attributes! ||
                                    item.ThumbnailUrl != itemOld.ThumbnailUrl ||
                                    item.Videos.count != itemOld.Videos.count) {
                                    
                                    if((self.viewMultiangleCameraIsActive && !self.viewMultiangleCameraHighlightedIsActive)){
                                        
                                        if(item.Videos.count != itemOld.Videos.count) {
                                            //add item multicam
                                            if (previousMulticamView.tag != 0) {
                                                self.drawMulticam(item: item, isAdd: false, isUpdate: true, view: previousMulticamView)
                                            } else {
                                                self.drawMulticam(item: item, isAdd: true, isUpdate: false, view: UIView())
                                            }
                                        } else {
                                            //update item multicam
                                            
                                            let view = self.multiangleCameraScrollView.viewWithIDString(idstring: item.ID!)
                                            self.drawMulticam(item: item, isAdd: false, isUpdate: true,view: view!)
                                            view!.removeFromSuperview()
                                        }
                                        
                                    }
                                }
                            }
                            
                            if (self.viewMultiangleCameraHighlightedIsActive && item.ID == String(self.selectedPlayByPlay)) {
                                
                            }
                        }
                    }
                    
                    
                    if (!isPresent) {
                        
                        if(self.viewMultiangleCameraIsActive && !self.viewMultiangleCameraHighlightedIsActive){
                            
                            //add item multicam
                            if (previousMulticamView.tag != 0) {
                                self.drawMulticam(item: item, isAdd: false, isUpdate: true, view: previousMulticamView)
                            } else {
                                self.drawMulticam(item: item, isAdd: true, isUpdate: false, view: UIView())
                            }
                            
                        }
                    }
                    
                    if (self.viewMultiangleCameraIsActive && !self.viewMultiangleCameraHighlightedIsActive) {
                        let multi = self.multiangleCameraScrollView.viewWithIDString(idstring: item.ID!)
                        if(multi != nil){
                            previousMulticamView = multi!
                        }
                    }
                }
                
                
            }
            for item in oldclips {
                var isPresent = false
                
                for itemNew in newclips {
                    if (item.ID == itemNew.ID) {
                        isPresent = true
                    }
                }
                
                if (!isPresent) {
                    if ((self.viewMultiangleCameraIsActive && !self.viewMultiangleCameraHighlightedIsActive)) {
                        //remove item multicam
                        let viewMulti = self.multiangleCameraScrollView.viewWithIDString(idstring: item.ID!)
                        viewMulti!.removeFromSuperview()
                    }else if(self.viewMultiangleCameraHighlightedIsActive && item.ID == String(self.selectedPlayByPlay)){
                        self.delegate?.goBack()
                        multiangleCameraScrollView.isHidden = false
                    }
                    
                }
            }
        }
        
        if(oldclips.count==0 && newclips.count>0 && self.viewMultiangleCameraIsActive){
            if (self.view.subviews.contains(self.warningLabel)){
                self.warningLabel.removeFromSuperview()
            }
            self.spinnerMulticam.startAnimating()
            self.showMulticameraCamera()
        }
        
        //CALCULATE SPACES SCROLLVIEWS
        
        if((self.viewMultiangleCameraIsActive && !self.viewMultiangleCameraHighlightedIsActive)){
            var multicamOffsetWidth: CGFloat = -100
            for item in oldclips {
                let view = self.multiangleCameraScrollView.viewWithIDString(idstring: item.ID!)
                if(view != nil){
                    view!.frame.origin.x = CGFloat(multicamOffsetWidth + 100)
                    multicamOffsetWidth += self.multicamItemWidth
                }
            }
        }
        
    }
    
    // MARK: -
    
    // MARK: Notifications data ready
    @objc func handleMulticam(notification: NSNotification){
        if(notification.name.rawValue  == "multicamDataReady"){
            NSLog("UPDATE MULTICAM")
            DispatchQueue.main.async {
                self.updateMulticam()
            }
        }
    }
    
}
