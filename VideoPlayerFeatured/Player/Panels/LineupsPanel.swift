//
//  LineupsPartialView.swift
//  AppleTV
//
//  Created by Corrado Amatore on 07/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

class LineupsPanel: BaseViewController{
    
    let lineupsScrollView = UIScrollView()
    
    var teamHomeStackView = UIStackView()
    var teamAwayStackView = UIStackView()
    
    var benchHomeStackView = UIStackView()
    var benchAwayStackView = UIStackView()
    
    let spinnerLineups = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    let warningLabel = UILabel()
    
    var lineupsObj: LineupModel = LineupModel()
    let screenRect = UIScreen.main.bounds
    
    var playerContainerView = UIView()
    var translationData: Translations?
    
    var urlEventsIcons: String = ""
    
    var parameters = ParametersModel()
    
    var cup:Int = 0
    var season:Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    var matchId: Int = 0
    var skin: String = ""
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    init(parameters: ParametersModel, urlEventsIcons: String, skin: String){
        super.init(nibName: nil, bundle: nil)
        self.parameters = parameters
        self.cup = Int(parameters.Cup!)!
        self.season = Int(parameters.Season!)!
        self.roundId = Int(parameters.RoundId!)!
        self.matchDay = Int(parameters.Matchday!)!
        self.matchId = Int(parameters.MatchId!)!
        self.urlEventsIcons = urlEventsIcons
        self.skin = skin
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        self.view.frame = CGRect(x: 0, y: 0, width: 770, height: 3840)
        spinnerLineups.frame = CGRect(x: 380, y: self.screenRect.height / 2 - 20, width: 40, height: 40)
        
        DispatchQueue.main.async() {
            self.view.addSubview(self.spinnerLineups)
            self.spinnerLineups.startAnimating()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(LineupsPanel.handleLineups(notification:)), name: NSNotification.Name(rawValue: "lineupsDataReady"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lineupsObj = DataProvider.sharedInstance.getMatchLineups(parameters: parameters)
        //translationData = DataProvider.sharedInstance.getTranslation(cup: cup, season: season)
        
        DispatchQueue.main.async() {
            self.drawLineups()
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        NSLog("Dismiss panel animated")
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "lineupsDataReady"), object: nil)
    }
    
    
    func drawLineups(){
        //self.lineupsScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        var home = 0
        for playerHome:PlayerModel in self.lineupsObj.Homes! {
            if(playerHome.isLineup() || playerHome.IsTeam ){
                drawPlayer(player: playerHome,teamStackView: teamHomeStackView)
            }else if(playerHome.isBench()){
                if (home < 11)
                {
                    drawPlayer(player: playerHome,teamStackView: benchHomeStackView)
                    home = home + 1
                }
            }
        }
        
        //teamAwayStackView = UIStackView()
        //benchAwayStackView = UIStackView()
        var away = 0
        for playerAway:PlayerModel in self.lineupsObj.Aways! {
            if(playerAway.isLineup() || playerAway.IsTeam ){
                drawPlayer(player: playerAway,teamStackView: teamAwayStackView)
            }else if(playerAway.isBench()){
                if (away < 11)
                {
                    drawPlayer(player: playerAway,teamStackView: benchAwayStackView)
                    away = away + 1
                }
            }
        }
        
        
        if(self.lineupsObj.Homes!.count<=1 && self.lineupsObj.Aways!.count<=1){
            warningLabel.frame = CGRect(x: 200, y: self.screenRect.height / 2 - 20, width: 500, height: 40)
            warningLabel.font = UIFont.init(name: "Dosis-Regular", size: 28)
            warningLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            warningLabel.text = String("Lineups coming soon.").uppercased()
            self.view.addSubview(warningLabel)
            
        }
        settingUpControlsLineups()
    }
    
    func settingUpControlsLineups(){
        
        let homeHeight = teamHomeStackView.arrangedSubviews.count * 80
        let awayHeight = teamAwayStackView.arrangedSubviews.count * 80
        let benchHomeHeight = benchHomeStackView.arrangedSubviews.count * 80
        let benchAwayHeight = benchAwayStackView.arrangedSubviews.count * 80
        
        let leftHome = 0
        let leftAway = 330
        
        let homeContainerButton = UIButton()
        let awayContainerButton = UIButton()
        let benchHomeContainerButtton = UIButton()
        let benchAwayContainerButton = UIButton()
        
        
        homeContainerButton.frame = CGRect(x: leftHome, y: 20, width: 320, height: homeHeight)
        awayContainerButton.frame = CGRect(x: leftAway, y: 20, width: 320, height: awayHeight)
        benchHomeContainerButtton.frame = CGRect(x: leftHome, y: 1300, width: 320, height: benchHomeHeight)
        benchAwayContainerButton.frame = CGRect(x: leftAway, y: 1300, width: 320, height: benchAwayHeight)
        
        teamHomeStackView.frame = CGRect(x: 0, y: 20, width: 320, height: homeHeight)
        teamAwayStackView.frame = CGRect(x: 0, y: 20, width: 320, height: awayHeight)
        benchHomeStackView.frame = CGRect(x: 0, y: 20, width: 320, height: benchHomeHeight)
        benchAwayStackView.frame = CGRect(x: 0, y: 20, width: 320, height: benchAwayHeight)
        
        homeContainerButton.addSubview(teamHomeStackView)
        awayContainerButton.addSubview(teamAwayStackView)
        benchHomeContainerButtton.addSubview(benchHomeStackView)
        benchAwayContainerButton.addSubview(benchAwayStackView)
        
        let arrowDown = UIImageView(image: UIImage(named: "ArrowDown"))
        arrowDown.frame = CGRect(x: 280, y:1020, width: 60, height: 60)
        
        let arrowUp = UIImageView(image: UIImage(named: "ArrowUp"))
        arrowUp.frame = CGRect(x:280,  y:1100, width: 60, height:60)
        
        let titleFrame = CGRect(x: 200, y: 1150, width: 300, height: 50)
        let titleLabel = UILabel(frame: titleFrame)
        titleLabel.text = "SUBSTITUTES"
        titleLabel.font = UIFont.init(name: "Dosis-Regular", size: 32)
        titleLabel.textAlignment = NSTextAlignment.left
        titleLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        
        
        lineupsScrollView.addSubview(homeContainerButton)
        lineupsScrollView.addSubview(awayContainerButton)
        lineupsScrollView.addSubview(arrowDown)
        lineupsScrollView.addSubview(arrowUp)
        lineupsScrollView.addSubview(titleLabel)
        lineupsScrollView.addSubview(benchHomeContainerButtton)
        lineupsScrollView.addSubview(benchAwayContainerButton)
        
        
        lineupsScrollView.frame = CGRect(x: 70, y: 0, width: 700, height: screenRect.height)
        lineupsScrollView.layoutMargins = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        lineupsScrollView.contentSize = CGSize(width: 700, height: 2030 + 150)
        //lineupsScrollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        //lineupsScrollView.scrollEnabled = true
        //lineupsScrollView.clipsToBounds = false
        
        self.view.addSubview(lineupsScrollView)

        UserExperienceUtility.animateSubviewsHorizontally(view: teamHomeStackView, dimension: -2000, duration: 0.5)
        UserExperienceUtility.animateSubviewsHorizontally(view: benchHomeStackView, dimension: -2000, duration: 0.5)
        UserExperienceUtility.animateSubviewsHorizontally(view: teamAwayStackView, dimension: -2000, duration: 0.5)
        UserExperienceUtility.animateSubviewsHorizontally(view: benchAwayStackView, dimension: -2000, duration: 0.5)
        
        if (spinnerLineups.isAnimating) {
            spinnerLineups.stopAnimating()
        }
        
    }
    
    
    
    func drawPlayer(player:PlayerModel, teamStackView: UIStackView){
        
        var offsetY = CGFloat(integerLiteral: -40)
        
        if ((player.IsTeam) != nil && player.IsTeam){
            offsetY = CGFloat(integerLiteral: 0)
        }
        
        let imagesWidth = CGFloat(integerLiteral: 60)
        let imagesHeight = CGFloat(integerLiteral: 60)
        let imagesFrame = CGRect(x: 0, y: offsetY, width: imagesWidth, height: imagesHeight)
        let numberFrame = CGRect(x: imagesWidth + 2, y: offsetY, width: 55, height: imagesHeight)
        let nameFrame = CGRect(x: 120, y: -57, width: 200, height: imagesHeight)
        
        
        playerContainerView = UIView()
        playerContainerView.frame=CGRect(x: 0, y: 0, width: 320, height: imagesHeight+5)
        playerContainerView.tag = player.ID!
        
        var imgUrl = player.ThumbLarge!
        if(imgUrl.characters.count==0){
            imgUrl = player.ThumbMedium!
        }
        if(imgUrl.characters.count==0){
            imgUrl = player.ThumbSmall!
        }
        if(imgUrl.characters.count > 0){
            
            
            if ((player.IsTeam) != nil && player.IsTeam){
                
                let img = Utility.GetCachedImage(key: "lineup_team_" + (player.ID?.description)!, url: imgUrl)
                if(img != nil){
                    
                    let imgView = UIImageView(image: img) // UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
                    
                    imgView.frame = CGRect(x: 0, y: offsetY, width: 60, height: 60)
                    
                    //imgView.adjustsImageWhenAncestorFocused = true
                    imgView.contentMode = .scaleAspectFill
                    
                    playerContainerView.addSubview(imgView)
                }
            }else{
                let img = Utility.GetCachedImage(key: "lineup_player_" + (player.ID?.description)!, url: imgUrl)
                if(img != nil){
                    let imgView = UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: imagesWidth, height: imagesHeight), withAplha: 1))
                    
                    imgView.frame = imagesFrame
                    imgView.layer.cornerRadius = imgView.frame.size.width/2
                    imgView.clipsToBounds = true
                    
                    //imgView.adjustsImageWhenAncestorFocused = true
                    imgView.contentMode = .scaleAspectFill
                    
                    playerContainerView.addSubview(imgView)
                }
            }
            
            
            
            
            
            
        }
        
        
        let numberLabel = UILabel(frame: numberFrame)
        numberLabel.text = String("\(player.BibNumber!)")
        numberLabel.font = UIFont.init(name: "Dosis-Regular", size: 38)
        numberLabel.textAlignment = NSTextAlignment.center
        numberLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        numberLabel.lineBreakMode = NSLineBreakMode.byTruncatingMiddle
        numberLabel.numberOfLines = 0
        
        numberLabel.layer.shadowColor = UIColor.black.cgColor
        numberLabel.layer.shadowOffset = .zero
        numberLabel.layer.shadowRadius = 2
        numberLabel.layer.shadowOpacity = 1
        
        if ((player.IsTeam) != nil && !player.IsTeam && player.events != nil){
            managePlayerEvents(player: player, container: playerContainerView)
        }
        //imgCards.layer.cornerRadius = numberLabel.frame.size.width/2
        
        
        let nameLabel = UILabel(frame: nameFrame)
        
        var shortname = ""
        
        if let sur = player.OfficialSurname{
            shortname = sur
        }
        
        if ((player.IsTeam) != nil && player.IsTeam){
            numberLabel.numberOfLines = 2
            nameLabel.font = UIFont.init(name: "Dosis-Semibold", size: 34)
            nameLabel.frame = CGRect(x: 80, y: 0, width: 240, height: 70)
            if shortname != nil{
                nameLabel.text =  shortname.uppercased()
            }
        }else{
            
            //NAME
            let name = player.OfficialName as String?
            var nameStr = ""
            if(name != nil && name!.characters.count>0){
                nameStr = (name! as NSString).substring(to: 1) + String(". ")
            }
            //SURNAME
            var surname =  ""
            if let sur = player.OfficialSurname {
                surname = sur
            }
            let playerid = "\(player.ID!)"
            
            nameLabel.text =  nameStr.uppercased() + surname.uppercased()
            if((player.IsGoalkeeper) != nil && player.IsGoalkeeper){
                nameLabel.text = nameLabel.text! + String(" (GK)").uppercased()
            }
            if((player.IsCaptain) != nil && player.IsCaptain){
                nameLabel.text = nameLabel.text! + String(" (C)").uppercased()
            }
            nameLabel.font = UIFont.init(name: "Dosis-Regular", size: 22)
        }
        nameLabel.textAlignment = NSTextAlignment.left
        nameLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        nameLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        nameLabel.numberOfLines = 0
        
        if(shortname != nil && shortname.characters.count > 0){
            let rightBorder = CALayer()
            rightBorder.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3).cgColor
            rightBorder.frame = CGRect(x: numberLabel.frame.width-5,y: 15,width: 1, height: 30)
            numberLabel.layer.addSublayer(rightBorder)
        }
        
        if ((player.IsTeam) != nil && !player.IsTeam){
            playerContainerView.addSubview(numberLabel)
        }
        
        playerContainerView.addSubview(nameLabel)
        
        if ((player.IsTeam) != nil && player.IsTeam){
            let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 100))
            emptyLabel.text = " "
            playerContainerView.addSubview(emptyLabel)
        }
        
        teamStackView.axis = UILayoutConstraintAxis.vertical
        teamStackView.distribution = UIStackViewDistribution.fillEqually
        teamStackView.alignment = UIStackViewAlignment.leading
        //teamHomeStackView.spacing = CGFloat(10)
        
        //teamStackView.layoutMargins = UIEdgeInsets(top: 30, left: 0, bottom: 10, right: 0)
        //teamStackView.layoutMarginsRelativeArrangement = true
        
        //teamHomeStackView.contentSize = teamHomeStackView.bounds.size
        //teamStackView.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        //teamHomeStackView.scrollEnabled = false
        //teamStackView.clipsToBounds = false
        
        teamStackView.addArrangedSubview(playerContainerView)
        
        //TRICK EMPTY ROW
        if ((player.IsTeam) != nil && player.IsTeam){
            playerContainerView = UIView()
            let numberLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 1))
            playerContainerView.addSubview(numberLabel)
            teamStackView.addArrangedSubview(playerContainerView)
        }
        
    }
    
    func managePlayerEvents(player: PlayerModel, container: UIView){
        let imageWidth = 11
        let imageHeight = 20
        
        
        let playerEventsStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: 320, height: imageHeight+5))
        playerEventsStackView.tag = 4000
        playerEventsStackView.axis = UILayoutConstraintAxis.horizontal
        playerEventsStackView.distribution  = UIStackViewDistribution.fill
        playerEventsStackView.alignment = UIStackViewAlignment.leading
        //playerEventsStackView.spacing = 20
        var eventsIndex = 0
        for eve in player.events!
        {
            if(eve.phaseId == 5){
                return
            }
            var imgView: UIImageView
            var urlIcon: String = urlEventsIcons + eve.eventType! + ".png"
            var thumbName = "lineup_events_\(eve.eventType!)"
            
            if(eve.eventType == "Substitution"){
                if(eve.playerFromId == player.ID){
                    urlIcon = urlEventsIcons + eve.eventType! + "_out.png"
                    thumbName = "lineup_events_\(eve.eventType!)_out"
                }else if(eve.playerToId == player.ID){
                    urlIcon = urlEventsIcons + eve.eventType! + "_in.png"
                    thumbName = "lineup_events_\(eve.eventType!)_in"
                }
            }
            if(urlIcon.characters.count>0){
                
                let img = Utility.GetCachedImage(key: thumbName, url: urlIcon)
                if(img != nil){
                    imgView = UIImageView(image: img!)
                    let iconFrame = CGRect(x: CGFloat(30 * eventsIndex), y: 0, width: (img?.size.width)!, height: (img?.size.height)!)
                    imgView.frame = iconFrame
                    eventsIndex += 1
                    playerEventsStackView.addSubview(imgView)
                }
                
                
            }
            
        }
        
        let stackIconsFrame = CGRect(x: 123, y: -10, width: eventsIndex * (imageWidth + 3) , height: imageHeight)
        playerEventsStackView.frame = stackIconsFrame
        container.addSubview(playerEventsStackView)
    }
    
    func checkAndUpdateLineupEvents(playersNew: [PlayerModel], playersOld: [PlayerModel]){
        
        for playerNew in playersNew{
            var eventfound = false
            for playerOld in playersOld{
                if(playerOld.ID == playerNew.ID && playerNew.events != nil && playerOld.events != nil){
                    if(playerNew.events?.count != playerOld.events?.count){
                        UpdateEvents(player: playerNew)
                        eventfound = false
                        break
                    }
                    for eventNew in playerNew.events!{
                        for eventOld in playerOld.events!{
                            if(eventNew.id == eventOld.id){
                                eventfound = true
                            }
                        }
                        if(eventfound == false){
                            UpdateEvents(player: playerNew)
                            break
                        }
                    }
                    if(eventfound == false){
                        break
                    }
                }
                
            }
        }
        
    }
    
    func UpdateEvents(player: PlayerModel){
        let container = self.view.viewWithTag(player.ID!)
        
        let stackevents = container?.viewWithTag(4000)
        
        stackevents?.removeFromSuperview()
        if  let container = container{
            managePlayerEvents(player: player, container: container)
        }
        
    }
    
    func updateLineups() {
        if ((DataProvider.sharedInstance.newLineups.Homes?.count)! > 0 && (DataProvider.sharedInstance.newLineups.Aways?.count)! > 0) {
            //self.homeTeamName = (lineupsObj.Homes![0] as PlayerModel).OfficialSurname!
            //self.awayTeamName = (lineupsObj.Aways![0] as PlayerModel).OfficialSurname!
        }
        //if(self.viewLineupsIsActive){
        self.checkAndUpdateLineupEvents(playersNew: DataProvider.sharedInstance.newLineups.Homes!, playersOld: DataProvider.sharedInstance.oldLineups.Homes!)
        self.checkAndUpdateLineupEvents(playersNew: DataProvider.sharedInstance.newLineups.Aways!, playersOld: DataProvider.sharedInstance.oldLineups.Aways!)
        //}
        
        //self.lineupsObj = lineupsObj
    }
    
    
    // MARK: -
    
    // MARK: Notifications data ready
    @objc func handleLineups(notification: NSNotification){
        if(notification.name.rawValue == "lineupsDataReady"){
            
            NSLog("UPDATE LINEUPS")
            DispatchQueue.main.async {
                self.updateLineups()
            }
            //actualMatchObj = DataProvider.sharedInstance.getMatch(cup, season: season, roundId: roundId, matchDay: matchDay, matchId: matchId)
        }
    }
    
}


