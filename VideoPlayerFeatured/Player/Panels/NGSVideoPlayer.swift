//
//  NGSVideoPlayer.swift
//  D3OTTCommon
//
//  Created by Corrado Amatore on 03/10/2017.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class NGSVideoPlayer{
    
    let secret: String = "f197181ca63f24eec6fe5fa7733c55435316534af280783453fba3bfa540722d" //NGS: "01650495985D7627C9109F095D521C3F"
    var avPlayer = AVPlayer()
    var parent = UIViewController()
    public let avPlayerController = NGSAVPlayerViewController()
   
    //lazy var topView = UIView(frame:  CGRect(x:0, y:0, width:1920, height:1080))
    
    public func Initialize(parentViewController: UIViewController, parentView: UIView, enableControls: Bool = false){
        
        parent = parentViewController
        
        avPlayerController.removeFromParentViewController()
        avPlayerController.view.removeFromSuperview()
        

        if(enableControls){
        
            EnableControls()
        
        }else{
            avPlayerController.showsPlaybackControls = false
            avPlayerController.view.isUserInteractionEnabled = false
        }
        
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = parentViewController.view.bounds
        
        
        avPlayerController.view.backgroundColor = UIColor.clear
        avPlayerController.view.layer.shadowColor = UIColor.black.cgColor
        avPlayerController.view.layer.shadowOpacity = 0.8
        avPlayerController.view.layer.shadowOffset = .zero
        avPlayerController.view.layer.shadowRadius = 25
        avPlayerController.view.layer.shadowPath = UIBezierPath(rect: avPlayerController.view.bounds).cgPath
        avPlayerController.view.layer.shouldRasterize = false
        avPlayerController.view.transform = CGAffineTransform.identity
        avPlayerController.videoGravity = "AVLayerVideoGravityResize"
        avPlayerController.view.clipsToBounds = false
        
       
        parent.addChildViewController(avPlayerController)
        parentView.addSubview(avPlayerController.view)
        
        
    }
    
    func EnableControls(){
        
        avPlayerController.showsPlaybackControls = true
        if #available(tvOS 10.0, *) {
            avPlayerController.isSkipForwardEnabled = false
            avPlayerController.isSkipBackwardEnabled = false
        }        
        avPlayerController.requiresFullSubtitles = false
        avPlayerController.view.isUserInteractionEnabled = true
        
        if #available(tvOS 11.0, *) {
            avPlayerController.playbackControlsIncludeInfoViews = false
        } else {
            // Fallback on earlier versions
        }
        if #available(tvOS 11.0, *) {
            //avPlayerController.playbackControlsIncludeTransportBar = false
        } else {
            // Fallback on earlier versions
        }
    }

    
    @objc func pauseRecognizer(recognizer: UITapGestureRecognizer) {
        if (avPlayer.isPlaying) {
            //avPlayerController.showsPlaybackControls = true
            avPlayer.pause()
        }else{
            //avPlayerController.showsPlaybackControls = false
            avPlayer.play()
        }
    }
    
    func SelectAudio(lang: String) {
        
        if let item = avPlayer.currentItem, item.tracks(type: .audio).count>0 {
            let audios = item.tracks(type: .audio)
            if(audios.contains(lang)){
                let success = avPlayer.currentItem?.select(type: .audio, name: lang)
                print("Audio switch to " + lang)
            }else{
                print("Audio switch to " + audios.first!)
            }
        }
    }
    
    func CreatePlayerItem(url: String) -> AVPlayerItem {
        
        let urlTokenized: String = AkamaiTokenizer.tokenize(url: url, secret: secret)
        
        let url = NSURL(string: urlTokenized)
        let avAsset = AVAsset(url: url! as URL)
        let avPlayerItem = AVPlayerItem(asset: avAsset)
        
        return avPlayerItem
    }

    
    // MARK: -
    
    // MARK: Public functions
    
    public func SetVideo(url: String, seek: Double = 0){
        if(!url.isEmpty){
            
            let res = CreatePlayerItem(url: url)
            SetVideo(item: res, seek: seek)
        }
    }
    
    public func SetVideo(item: AVPlayerItem, seek: Double = 0){
        avPlayer.pause()
        avPlayer.seek(to: kCMTimeZero)
        avPlayer.replaceCurrentItem(with: item)
        
        SelectAudio(lang: "English")
        
        if (seek == 0) {
            Seek(time: kCMTimeZero)
        } else {
            let time = CMTimeMakeWithSeconds(seek, 1000)
            Seek(time: time)
        }
    }
    
    public func Seek(time: CMTime = kCMTimeZero){
        avPlayer.pause()
        avPlayer.currentItem?.seek(to: time)
        avPlayer.play()
    }
    
    
    public func Play(){
        avPlayer.play()
    }
    
    public func Pause(){
        avPlayer.pause()
    }
    
    public func Hide(){
        avPlayerController.view.alpha = 0.0
    }
    public func Show(){
        avPlayerController.view.alpha = 1.0
    }
    
    public func Mute(){
        avPlayer.isMuted = true
    }
    
    public func Duration() -> CMTime{
        return avPlayer.currentItem!.asset.duration
    }
    
    public func PlaybackType() -> String{
        return (avPlayer.currentItem?.accessLog()?.events.first?.playbackType)!
    }
    
    public func PlayerRate() -> Float {
        return avPlayer.rate
    }
    
    public var ShowControls: Bool{
        get{
            
           return avPlayerController.showsPlaybackControls
        }
        set{
            avPlayerController.showsPlaybackControls = newValue
        }
    }
    public func InteractionStop(){
        avPlayerController.view.isUserInteractionEnabled = false
    }
    @objc public func InteractionStart(){
        
        EnableControls()
    }
    
//    func override deinit() {
//        self.avPlayer.removeObserver(self, forKeyPath: "status")
//        self.avPlayer.removeObserver(self, forKeyPath: "currentItem.status")
//    }

}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

class NGSAVPlayerViewController: AVPlayerViewController{
    public func IsFullScreen() -> Bool {
        let screenRect = UIScreen.main.bounds
        if(self.view.frame.width == screenRect.width && self.view.frame.height == screenRect.height){
            return true
        }
        return false
    }
    
}
