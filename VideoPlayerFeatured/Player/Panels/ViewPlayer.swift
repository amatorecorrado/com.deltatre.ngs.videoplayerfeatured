//
//  ViewPlayer.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 15/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

//
//  ViewController.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 08/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

public class ViewPlayer: BaseViewController, NotificationPanelDelegate, TodayMatchesPanelDelegate, MulticamPanelDelegate
{
    // MARK: -
    
    // MARK: Define variables
    
    var cup: Int = 0
    var cupSeason: Int = 0
    var season: Int = 0
    public var matchId: Int = 0
    public var videoUrl: String  = ""
    
    var parameters = ParametersModel()
    
    var currentVideoId = ""
    
    var seekTime: String = ""
    var videoStartTime: String = ""
    var counterPenalty = 1
    
    //var cupInfoUrl = NSURL()
    var matchIdSplitted: [String] = [String]()
    var matchDayScheduleUrl = NSURL()
    var lineupUrl = NSURL()
    
    var videoData = VideoDataModel()
    var actualMatchObj: Match? = Match()
    
    var selectedPlayByPlay: String = ""
    var selectedCameraCurrent: String = ""
    //var selectedSecondPlayerMatchID: Int = -1
    
    var homeTeamName: String = ""
    var awayTeamName: String = ""
    
    var canCloseTheView = true
    
    var isPictureInPicture = false
    var isTwoPlayerView = false
    var isMenuOn = false
    
    let touchToleranceLight = CGFloat(integerLiteral: 125)
    let touchToleranceMax = CGFloat(integerLiteral: 300)
    
    let screenRect = UIScreen.main.bounds
    var initView = UIView()
    //    var avPlayerView_MAIN = AVPlayerViewController()
    //    var avPlayer_MAIN = AVQueuePlayer()
    //    let avPlayerView_SECONDARY = AVPlayerViewController()
    //    var avPlayer_SECONDARY = AVQueuePlayer()
    
    var videoPlayer_MAIN = NGSVideoPlayer()
    var videoPlayer_SECONDARY = NGSVideoPlayer()
    
    var avPlayerTempWidth = CGFloat(0)
    var menuViewController = PlayerMenuViewController(nibName: "PlayerMenuViewController", bundle: nil)
    var scoreLabel = UILabel()
    
    var backgroundOtherMatchesImage = UIImage()
    var backgroundNotificationsImage = UIImage()
    var backgroundMulticamImage = UIImage()
    var backgroundLineupsImage = UIImage()
    let titleMatchesLabel = UILabel()
    
    var touchCheckpointX = CGFloat()
    var touchCheckpointY = CGFloat()
    
    var menuOtherMatchesIsActive = false
    var viewOtherMatchesIsActive = false
    var menuNotificationsIsActive = false
    var viewNotificationsIsActive = false
    var menuMultiangleCameraIsActive = false
    var viewMultiangleCameraIsActive = false
    var viewMultiangleCameraHighlightedIsActive = false
    var menuLineupsIsActive = false
    var viewLineupsIsActive = false
    
    
    var dictMultiangleCameraOrder = Dictionary<String, VideoClipModel>()
    var dictMultiangleCameraAvPlayerQueue = Dictionary<String, AVPlayerItem>()
    
    var mainPlayerFrameStore: CGRect? = nil
    var secondaryPlayerFrameStore: CGRect? = nil
    var mainPlayerAnchorPointStore: CGPoint? = nil
    var secondaryPlayerAnchorPointStore: CGPoint? = nil
    
    var playerContainerView = UIView()
       
    var notificationPanel = NotificationPanel()
    var lineupsPanel = LineupsPanel()
    var todayMatchesPanel = TodayMatchesPanel()
    var multicamPanel = MulticamPanel()
    
    let mainSpinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    var timer: Timer? = Timer.init()
    
    // MARK: -
    
    // MARK: View Methods
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        let backRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewPlayer.backRecognizer(recognizer:)))
        backRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(backRecognizer)
        
        //        self.view.backgroundColor = UIColor.init(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
        //        mainSpinner.frame = CGRect(x: self.screenRect.width / 2 - 20, y: self.screenRect.height / 2 - 20, width: 40, height: 40)
        //        self.view.addSubview(self.mainSpinner)
        //        mainSpinner.startAnimating()
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        self.posticipateViewDidLoad()
        //        }
    }
    
    public override func observeValue(
        forKeyPath keyPath: String?,
        of object: Any?,
        change: [NSKeyValueChangeKey : Any]?,
        context: UnsafeMutableRawPointer?
        ) {
        if let keyPath = keyPath, let new = change?[.newKey], let kind = change?[.kindKey], let context = context{
            print("key:\(keyPath) new: \(new) kind \(kind), context: \(context)")
        }
    }
    
    func posticipateViewDidLoad() {
        //ngs_analytics_setView(videoUrl,matchId: String(matchId),viewName: "PlayerMatchView")
        
        //TEST
        //isLive = true
        
        // Do any additional setup after loading the view, typically from a nib.
        //self.view.isUserInteractionEnabled = true
        self.videoPlayer_MAIN.avPlayer.addObserver(self, forKeyPath:"currentItem.status", options:[.initial, .new], context:nil)
        self.videoPlayer_MAIN.avPlayer.addObserver(self, forKeyPath:"status", options:[.initial, .new], context:nil)
        
        InitData()
        matchDayScheduleUrl = NSURL(string: Utility.ReplaceParameters(text: (viewConfiguration?.feeds["feedMatchdaySchedule"])!, parameters: parameters))!
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundMatches = NSURL(string: (viewConfiguration?.images.getValue(key: "backgroundMatches"))!)
        let dataBackgroundMatches = NSData(contentsOf: urlBackgroundMatches! as URL)
        UIImage(data: dataBackgroundMatches! as Data)?.draw(in: self.view.bounds)
        backgroundOtherMatchesImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundNotifications = NSURL(string: (viewConfiguration?.images.getValue(key: "backgroundNotifications"))!)
        let dataBackgroundNotifications = NSData(contentsOf: urlBackgroundNotifications! as URL)
        UIImage(data: dataBackgroundNotifications! as Data)?.draw(in: self.view.bounds)
        backgroundNotificationsImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundMulticam = NSURL(string: (viewConfiguration?.images.getValue(key: "backgroundMulticam"))!)
        let dataBackgroundMulticam = NSData(contentsOf: urlBackgroundMulticam! as URL)
        UIImage(data: dataBackgroundMulticam! as Data)?.draw(in: self.view.bounds)
        backgroundMulticamImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let urlBackgroundLineups = NSURL(string: (viewConfiguration?.images.getValue(key: "backgroundLineups"))!)
        let dataBackgroundLineups = NSData(contentsOf: urlBackgroundLineups! as URL)
        UIImage(data: dataBackgroundLineups! as Data)?.draw(in: self.view.bounds)
        backgroundLineupsImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        initView.backgroundColor = UIColor.init(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
        initView.frame = self.view.layer.bounds
        //initView.isUserInteractionEnabled = true
        
        
        
        videoPlayer_MAIN.Initialize(parentViewController: self, parentView: initView, enableControls: true)

        //        DefineMainPlayer()
        
        let otherMatchesImgView = UIImageView(image: UIImage(named: "MockOtherMatches"))
        otherMatchesImgView.frame = CGRect(x:0, y:0, width:1920,height: 1080)
        otherMatchesImgView.tag = 2000
        otherMatchesImgView.isHidden = true
        initView.addSubview(otherMatchesImgView)
        
        let notificationsImgView = UIImageView(image: UIImage(named: "MockNotifications"))
        notificationsImgView.frame = CGRect(x:0, y:0,width: 1920,height: 1080)
        notificationsImgView.tag = 3000
        notificationsImgView.isHidden = true
        initView.addSubview(notificationsImgView)
        
        let lineupsImgView = UIImageView(image: UIImage(named: "MockLineups"))
        lineupsImgView.frame = CGRect(x:0, y:0, width: 1920,height: 1080)
        lineupsImgView.tag = 1000
        lineupsImgView.isHidden = true
        initView.addSubview(lineupsImgView)
        
        
        DefineSecondaryPlayer()
        //        DefineSecondaryPlayer()
        
        
        self.view.addSubview(initView)
        
        InitAvPlayer()
        
        //drawScorePanel()
    }
    
    func DefineSecondaryPlayer(){
        videoPlayer_SECONDARY.Initialize(parentViewController: self, parentView: initView, enableControls: true)
        videoPlayer_SECONDARY.avPlayerController.view.isHidden = true
        videoPlayer_SECONDARY.avPlayerController.view.alpha = 0.0
        videoPlayer_SECONDARY.avPlayerController.view.transform = videoPlayer_SECONDARY.avPlayerController.view.transform.scaledBy(x: 0.1, y: 0.1)
        videoPlayer_SECONDARY.avPlayerController.showsPlaybackControls = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(avPlayerItemDidPlayToEndTimeNotification), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
 
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    deinit {
        videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
        videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
        //        videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
        //        videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
        
        //NSLog("deinit")
    }
    
    
    func drawScorePanel(){
        self.homeTeamName = (actualMatchObj?.HomeTeamName)!
        self.awayTeamName = (actualMatchObj?.AwayTeamName)!
        
        var scoreAway: String? = String(self.actualMatchObj!.Results.AwayGoals)
        var scoreHome: String? = String(self.actualMatchObj!.Results.HomeGoals)
        
        if (scoreHome == nil) {
            scoreHome = "0"
        }
        if (scoreAway == nil) {
            scoreAway = "0"
        }
        
        //let fontTeam = UIFont.init(name: "Dosis-Semibold", size: 40)
        //let fontPlayers = UIFont.init(name: "Dosis-Regular", size: 36)
        let home: String? =  self.actualMatchObj!.HomeTeamName
        let away: String? =  self.actualMatchObj!.AwayTeamName
        let scoreTeam = home!.uppercased() + "  " + scoreHome! + " - " + scoreAway! + "  " + away!.uppercased()
        //let scorePlayers = NSMutableAttributedString(string: "FERNANDO (RMA) 20'", attributes: [NSFontAttributeName: fontPlayers!, NSForegroundColorAttributeName: UIColor.lightGrayColor()])
        //scoreTeam.appendAttributedString(scorePlayers)
        self.scoreLabel.font = UIFont.init(name: "Dosis-Semibold", size: 40)
        self.scoreLabel.text = scoreTeam
        
    }
    
    func InitData(){
        
        
        
        // ---- DEFINE PROVIDER DATA
        DataProvider.sharedInstance.setParameters(configuration: self.viewConfiguration!)
        
        readParameters(config: self.viewConfiguration)
        
        cup = Int(((viewConfiguration!.parameters["cup"]))!)!
        //cupSeason = Int((viewConfiguration?.parameters["cupSeason"])!)!
        season = Int((viewConfiguration?.parameters["season"])!)!
        
        //OVERRIDE PASSING VALUE WITH CONFIGURATION ONE
        if let overrideMatchId = parameters.MatchId{
            self.matchId = Int(overrideMatchId)!
        }
        
        actualMatchObj = DataProvider.sharedInstance.getMatchData(parameters: parameters)
        if let rId = actualMatchObj?.RoundId{
            parameters.RoundId = String(describing: rId)
        }
        if let md = actualMatchObj?.MatchDay{
            parameters.Matchday = String(describing: md)
        }
        
        matchIdSplitted = Utility.getMatchIdSplitted(matchId: matchId)
        
    }
    
    func readParameters(config: ConfigurationModel?){
        var params = ParametersModel()
        
        params.MatchId = String(describing: self.matchId)
        
        if let config = config{
            if let data = config.parameters.getValue(key: "cup"), data.count > 0{
                params.Cup = data
            }
            if let data = config.parameters.getValue(key: "season"), data.count > 0{
                params.Season = data
            }
            if let data = config.parameters.getValue(key: "cupSeason"), data.count > 0{
                params.CupSeason = data
            }
            if let data = config.parameters.getValue(key: "matchId"), data.count > 0{
                params.MatchId = data
            }
            if let data = config.parameters.getValue(key: "cupCode"), data.count > 0{
                params.CupCode = data
            }
            
        }
        
        self.parameters = params
        
    }
    
    internal func InitAvPlayer() {
        
        self.scoreLabel.text = ""
        
        
        if let url = viewConfiguration!.feeds["feedLineups"] {
            lineupUrl = NSURL(string: Utility.ReplaceParameters(text: url, parameters: parameters))!
        }
        
        if (currentVideoId.count > 0) {
            videoData = VideoDataController().getVideoData(feedVideoData: viewConfiguration!.feeds["feedVideodata"]!, videoId: String(currentVideoId))!
            
            if (videoData.videoUrl.isEmpty == false) {
                matchId = Int(videoData.eventId)!
                videoUrl = videoData.videoUrl
                seekTime = ""
                videoStartTime = videoData.timeCodeIn
            }
        }
        
        if (!videoUrl.isEmpty) {
            
            
            let urlTokenized: String = AkamaiTokenizer.tokenize(url: videoUrl, secret: secret)
            
            let url = NSURL(string: urlTokenized)
            let avAsset = AVAsset(url: url! as URL)
            let avPlayerItem = AVPlayerItem(asset: avAsset)
            
            avAsset.mediaSelectionGroup(forMediaCharacteristic: )
            
            if(videoPlayer_MAIN.PlayerRate() != 0) {
                
                videoPlayer_MAIN.Pause()
                videoPlayer_MAIN.Seek(time: kCMTimeZero)
            }
            //videoPlayer_MAIN.avPlayerController.removeAllItems()
            videoPlayer_MAIN.SetVideo(item: avPlayerItem)
            videoPlayer_MAIN.Play()
            //videoPlayer_MAIN.avPlayerController.insert(avPlayerItem, after: nil)
            
            if(actualMatchObj!.isLive()){
                //DataProvider.sharedInstance.fetchInterval = 60
                DataProvider.sharedInstance.fetchMatchData(parameters: parameters, fetchInterval: 60)
                
            }else{
                #if DEBUG
                    DataProvider.sharedInstance.fetchMatchData(parameters: parameters, fetchInterval: 60)
                #else
                    DataProvider.sharedInstance.fetchMatchData(parameters: parameters) //,fetchInterval: 60
                #endif
                
            }
            
        } else {
            //TO-DO: add alert when no videourl si present
        }
        if let audios = videoPlayer_MAIN.avPlayer.currentItem?.tracks(type: .audio), audios.count > 0{
            if(audios.contains("english")){
                var success = videoPlayer_MAIN.avPlayer.currentItem?.select(type: .audio, name: "english")
                print("Audio switch to english " + String(describing: success))
            }else{
                var success = videoPlayer_MAIN.avPlayer.currentItem?.select(type: .audio, name: (audios.first)!)
                print("Audio switch to " + (audios.first)!)
            }
        }
    }
    
    
    
    
    @objc  func backRecognizer(recognizer: UITapGestureRecognizer) {
        backActions()
    }
    
    func backActions(){
        //NSLog("Menu Button Controller")
        
        if (canCloseTheView) {
            
            self.dismiss(animated: true, completion: nil)
            DataProvider.sharedInstance.dismissMatchData()
            navigationController?.popViewController(animated: true)
            
//            DataProvider.sharedInstance.dismissMatchData()
//
//            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
//
//                self.videoPlayer_MAIN.Hide()
//                self.videoPlayer_SECONDARY.Hide()
//
//
//            }, completion: {
//                (Value: Bool) in
////                self.videoPlayer_MAIN.Dismiss()
////                self.videoPlayer_SECONDARY.Dismiss()
//                self.videoPlayer_MAIN.Pause()
//                self.videoPlayer_SECONDARY.Pause()
//                self.videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
//                self.videoPlayer_MAIN.avPlayerController.view.removeFromSuperview()
//                self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
//                self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
//                Global.shared.isRefresh = true
//                NotificationCenter.default.removeObserver(self)
//                //self.navigationController?.popViewController(animated: true)
//                self.dismiss(animated: true, completion: nil)
//                //FormSheetRouterAction.close(source: self)
//            })
            
        }
        else {
            
            if (viewMultiangleCameraHighlightedIsActive) {
                multiangleRemove()
                goBackToMulticam()
                self.multicamPanel.multiangleCameraScrollView.isHidden = false
                viewMultiangleCameraHighlightedIsActive = false
                
            } else if (viewOtherMatchesIsActive) {
                todayMatchesPanel.view.removeFromSuperview()
                todayMatchesPanel.dismiss(animated: true, completion: nil)
                
                if(initView.subviews.contains(titleMatchesLabel)){
                    titleMatchesLabel.removeFromSuperview()
                }
                if (isTwoPlayerView) {
                    goBackMatches()
                    canCloseTheView = true
                    //goBackToPlayer()
                }
                else {
                    //SAVE ACTUAL STATE
                    mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                    mainPlayerAnchorPointStore = CGPoint(x: 0, y: 0)
                    canCloseTheView = true
                    goBackToPlayer()
                }
                
            } else if (viewMultiangleCameraIsActive) {
                
                dictMultiangleCameraAvPlayerQueue = [:]
                multicamPanel.view.removeFromSuperview()
                multicamPanel.dismiss(animated: true, completion: nil)
                goBackToPlayer()
            } else if (viewLineupsIsActive) {
                self.lineupsPanel.dismiss(animated: true, completion: nil)
                self.lineupsPanel.removeFromParentViewController()
                self.lineupsPanel.view.removeFromSuperview()
                goBackToPlayer()
                
            } else if (viewNotificationsIsActive) {
                notificationPanel.view.removeFromSuperview()
                notificationPanel.dismiss(animated: true, completion: nil)
                
                if(isPictureInPicture){
                    goBackNotification()
                }else{
                    //SAVE ACTUAL STATE
                    mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                    mainPlayerAnchorPointStore = CGPoint(x:0, y: 0)
                    goBackToPlayer()
                    
                }
            }
            else {
                goBackToPlayer()
            }
        }
    }
    
    
    func goBackToMulticam(){
        
        multicamPanel.panelMultiAngleRemove()
        
        videoPlayer_SECONDARY.Pause()
        self.videoPlayer_SECONDARY.avPlayerController.view.backgroundColor = UIColor.clear
        self.videoPlayer_MAIN.avPlayerController.view.isHidden = false
        
        self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
            
        }, completion: {
            (Value: Bool) in
            self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
            self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            
        })
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            //self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(1,1)
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.height)
            self.mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
            self.mainPlayerAnchorPointStore = CGPoint(x: 0, y: 0.5)
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.x = (self.screenRect.width-self.videoPlayer_MAIN.avPlayerController.view.frame.width) / 2
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.y = 40
            
            
        }, completion: {
            (Value: Bool) in
            self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = true
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 1
            self.videoPlayer_MAIN.Play()
        })
        
        
    }
    
    func goBackNotification(){
        
        isPictureInPicture = false
        
        videoPlayer_SECONDARY.Pause()
        self.videoPlayer_SECONDARY.avPlayerController.view.backgroundColor = UIColor.clear
        videoPlayer_MAIN.avPlayerController.view.isHidden = false
        
//        self.videoPlayer_SECONDARY.avPlayerController.view.layer.zPosition = 0
//        self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 1
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            //self.videoPlayer_SECONDARY.avPlayerController.view.transform = CGAffineTransformScale(self.videoPlayer_SECONDARY.avPlayerController.view.transform, 0.1, 0.1)
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
        }, completion: {
            (Value: Bool) in
            
            self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
            self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            
            
        })
        
        UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            //self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(0.1, 0.5)
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.height)//
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.x = 70
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.y = (self.screenRect.height-self.videoPlayer_MAIN.avPlayerController.view.frame.height) / 2
            
            
        }, completion: {
            (Value: Bool) in
            self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = true
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 1
            self.videoPlayer_MAIN.Play()
        })
        
        
        
        createNotificationPanel()
    }
    
    
    func goBackMatches(){
        
        isTwoPlayerView =  false
        
        videoPlayer_SECONDARY.Pause()
        self.videoPlayer_SECONDARY.avPlayerController.view.backgroundColor = UIColor.clear
        videoPlayer_MAIN.avPlayerController.view.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            //self.videoPlayer_SECONDARY.avPlayerController.view.transform = CGAffineTransformScale(self.videoPlayer_SECONDARY.avPlayerController.view.transform, 0.1, 0.1)
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
        }, completion: {
            (Value: Bool) in
            
            
            self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
            self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                
                //self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(1,1)
                self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: (self.mainPlayerFrameStore?.size.width)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.width, y: (self.mainPlayerFrameStore?.size.height)!/self.videoPlayer_MAIN.avPlayerController.view.frame.size.height)
                self.mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
                self.mainPlayerAnchorPointStore = CGPoint(x:0,y: 0.5)
                //self.videoPlayer_MAIN.avPlayerController.view.frame.origin.x = (self.screenRect.width-self.videoPlayer_MAIN.avPlayerController.view.frame.width) / 2
                //self.videoPlayer_MAIN.avPlayerController.view.frame.origin.y = 0
                self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 0
                
            }, completion: {
                (Value: Bool) in
                self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = true
                self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 1
                self.videoPlayer_MAIN.Play()
                
                
            })
        })
        
    }
    
    
    func goBackToPlayer() {
        
        self.lineupsPanel.view.isUserInteractionEnabled = false
        self.multicamPanel.view.isUserInteractionEnabled = false
        self.notificationPanel.view.isUserInteractionEnabled = false
        self.todayMatchesPanel.view.isUserInteractionEnabled = false
//
//        videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
//        videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
        
        if (!videoPlayer_SECONDARY.avPlayerController.view.isHidden) {
            self.videoPlayer_SECONDARY.avPlayerController.view.backgroundColor = UIColor.clear
            self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = true
            self.videoPlayer_SECONDARY.Pause()
            self.videoPlayer_MAIN.avPlayerController.view.isHidden = false
        }
        resetMenu()
        
        initView.viewWithTag(1000)?.isHidden = true
        initView.viewWithTag(2000)?.isHidden = true
        initView.viewWithTag(3000)?.isHidden = true
        
        let oldFrame = self.videoPlayer_MAIN.avPlayerController.view.frame
        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x:0, y:0)
        self.videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {

            self.videoPlayer_MAIN.avPlayerController.view.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
            self.videoPlayer_MAIN.avPlayerController.view.frame = self.initView.layer.bounds
            self.videoPlayer_MAIN.avPlayerController.view.layer.frame = self.initView.layer.bounds
            self.videoPlayer_MAIN.avPlayerController.view.isUserInteractionEnabled = true
            
        }, completion:  {  finish in
            
            if(self.isMenuOn){
                self.isMenuOn = false
                self.startInteraction()
            }
            self.videoPlayer_MAIN.Play()
            
        })
        
        viewOtherMatchesIsActive = false
        viewNotificationsIsActive = false
        viewMultiangleCameraIsActive = false
        viewMultiangleCameraHighlightedIsActive = false
        viewLineupsIsActive = false
        
        canCloseTheView = true
        
        
    }
    
    @objc func startInteraction(){
        self.videoPlayer_MAIN.avPlayerController.view.frame = UIScreen.main.bounds
        
        self.videoPlayer_SECONDARY.avPlayerController.view.layer.zPosition = 0
        self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
        self.videoPlayer_SECONDARY.avPlayerController.view.isUserInteractionEnabled = false
        
        self.initView.backgroundColor = UIColor.black
        
        self.videoPlayer_MAIN.InteractionStart()
//        self.videoPlayer_MAIN.avPlayerController.view.alpha = 1
//        self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 1001
//        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x:0, y:0)
//        self.videoPlayer_MAIN.avPlayerController.view.frame = self.initView.layer.bounds
//        self.videoPlayer_MAIN.avPlayerController.view.layer.frame = self.initView.layer.bounds
//        self.videoPlayer_MAIN.avPlayerController.view.isHidden = false
//        self.videoPlayer_MAIN.avPlayerController.view.transform = CGAffineTransform.identity
//        self.videoPlayer_MAIN.avPlayerController.videoGravity = "AVLayerVideoGravityResize"
//        self.videoPlayer_MAIN.avPlayerController.view.clipsToBounds = false
//        self.videoPlayer_MAIN.avPlayerController.view.isUserInteractionEnabled = true
        
        

    }
    
    public override weak var preferredFocusedView: UIView?
        {
        get{
            return videoPlayer_MAIN.avPlayerController.view
        }
    }
    
    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    @objc
    //    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    //
    //        if (keyPath == "rate") {
    //            //NSLog("rate \(videoPlayer_MAIN.avPlayerController.rate)")
    //            if(videoPlayer_MAIN.PlayerRate() == 0.0) {
    //                videoPlayer_MAIN.avPlayerController.view.isUserInteractionEnabled = true
    //            } else {
    //                videoPlayer_MAIN.avPlayerController.view.isUserInteractionEnabled = false
    //            }
    //        }
    //        if (keyPath == "status") {
    //            //NSLog("status \(videoPlayer_MAIN.avPlayerController.status.rawValue)")
    //            if(videoPlayer_MAIN.avPlayerController.status == AVPlayerStatus.unknown) {
    //                //NSLog("AVPlayerStatus.Unknown")
    //            } else if(videoPlayer_MAIN.avPlayerController.status == AVPlayerStatus.failed) {
    //                //NSLog("AVPlayerStatus.Failed")
    //            } else if(videoPlayer_MAIN.avPlayerController.status == AVPlayerStatus.readyToPlay) {
    //                //NSLog("AVPlayerStatus.ReadyToPlay")
    //                //if(videoPlayer_MAIN.avPlayerController.status == AVPlayerStatus.ReadyToPlay && videoPlayer_MAIN.avPlayerController.currentItem?.status == AVPlayerItemStatus.ReadyToPlay) {
    //                if(!actualMatchObj!.isLive()){
    //
    //                    //seekTime =   videoStartTime
    //                    if (videoData.trimIn <= 0) {
    //                        videoPlayer_MAIN.avPlayerController.currentItem?.seek(to: kCMTimeZero)
    //                    } else {
    //                        let time = CMTimeMakeWithSeconds(videoData.trimIn, 1000)
    //                        videoPlayer_MAIN.avPlayerController.currentItem?.seek(to: time)
    //                    }
    //                }else{
    //                    videoPlayer_MAIN.avPlayerController.currentItem?.seek(to: kCMTimeZero)
    //                }
    //
    //                videoPlayer_MAIN.avPlayerController.play()
    //                videoPlayer_MAIN.avPlayerController.view.isHidden = false
    //
    //                if(self.videoPlayer_MAIN.avPlayerController.currentItem?.accessLog()?.events.first?.playbackType == "LIVE"){
    //
    //                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "VideoStream", eventName: "Live")
    //                }else if(self.videoPlayer_MAIN.avPlayerController.currentItem?.accessLog()?.events.first?.playbackType == "VOD")
    //                {
    //                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "VideoStream", eventName: "Vod")
    //                }
    //
    //                print(videoPlayer_MAIN.avPlayerController.currentItem?.accessLog()?.events.first?.playbackType)
    //                //}
    //            }
    //        }
    //    }
    //
        @objc func avPlayerItemDidPlayToEndTimeNotification(notification: NSNotification) {
            
            //NSLog("avPlayerItemDidPlayToEndTimeNotification")
            if (!videoPlayer_SECONDARY.avPlayerController.view.isHidden && viewMultiangleCameraIsActive) {
                //print(videoPlayer_SECONDARY.avPlayerController.currentItem)
                let videoclip = cameraGetNextFromDictionary()
                //            let cameraname: String = videoclip.Camera!.Name!
                //multiangleCameraNameSelectedLabel.text = cameraname
                playMultiangleCameraAvPlayerAtIndex(cameraid: videoclip.ID!)
    
            }else {
                //print(videoPlayer_MAIN.avPlayerController.currentItem)
                videoPlayer_MAIN.Seek(time: kCMTimeZero)
                videoPlayer_MAIN.Play()
    
                if (self.mainSpinner.isAnimating)
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                    {
                        self.mainSpinner.stopAnimating()
                        self.mainSpinner.removeFromSuperview()
                    }
                }
            }
    
            DispatchQueue.main.async() {
                self.view.superview?.bringSubview(toFront: self.multicamPanel.view)
                self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
            }
        }
    
    
    func cameraGetNextFromDictionary() -> (VideoClipModel){
        var k: Int = 0;
        for (key, value) in dictMultiangleCameraOrder {
            if(value.ID == selectedCameraCurrent){
                if(key == String(dictMultiangleCameraOrder.count-1)){
                    k=0
                }else{
                    k = Int(key)! + 1
                }
                break
            }
        }
        
        return dictMultiangleCameraOrder[String(k)]!
    }
    
    func multicamGetDataFromDictionary(item: AVPlayerItem) -> (Int){
        var k: Int = 0;
        for (key, value) in dictMultiangleCameraAvPlayerQueue {
            if(value == item){
                k = Int(key)!
                break
            }
        }
        return k
    }
    
    public override func pressesChanged(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressChanged")
        
        let press = presses.first!
        //NSLog("type --> " + String(press.type.rawValue))
    }
    
    public override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("press")
        //self.videoPlayer_MAIN.InteractionStop()
        let press = presses.first!
        //NSLog("type --> " + String(press.type.rawValue))
    }
    
    public override func pressesEnded(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressEnded")
        let press = presses.first!
        //NSLog("type --> " + String(press.type.rawValue))
        
        if (canCloseTheView && videoPlayer_MAIN.PlayerRate() != 0) {
            
            if (menuViewController.view.alpha != 0 && (!viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewMultiangleCameraIsActive && !viewLineupsIsActive)) {
                
                if (menuOtherMatchesIsActive) {
                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "LiveMatches", linkID: "LiveMatches")
                    viewOtherMatches()
                }
                if (menuNotificationsIsActive) {
                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Notifications", linkID: "Notifications")
                    viewNotifications()
                }
                if (menuMultiangleCameraIsActive) {
                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Multicam", linkID: "Multicam")
                    viewMultiangleCamera()
                }
                if (menuLineupsIsActive) {
                    //ngs_analytics_setEvent(videoUrl, matchId: String(matchId), moduleName: "OpenPanel", eventName: "Lineups", linkID: "Lineups")
                    viewLineups()
                }
            } else {
                goBackToPlayer()
            }
            self.menuViewController.view.frame = self.view.bounds
            self.menuViewController.view.alpha = 0
            self.menuViewController.view.removeFromSuperview()
            self.menuViewController.removeFromParentViewController()
        }
    }
    
    public override func pressesCancelled(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        NSLog("pressCancelled")
        
        
        let press = presses.first!
        //NSLog("type --> " + String(press.type.rawValue))
    }
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touch")
        //NSLog("player rate --> \(videoPlayer_MAIN.avPlayerController.rate)")
        
        if (canCloseTheView && videoPlayer_MAIN.PlayerRate() != 0 && !viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewLineupsIsActive) {
            self.isMenuOn = true
            self.videoPlayer_MAIN.InteractionStop()
            
            let touch:UITouch = touches.first!
            let newLocation = touch.location(in: self.view)
            touchCheckpointX = newLocation.x
            touchCheckpointY = newLocation.y
            
            actualMatchObj = DataProvider.sharedInstance.getMatchData(parameters: parameters)
            
            //self.menuViewController = PlayerMenuViewController()
            //initView.addSubview(self.menuViewController.view)
            videoPlayer_MAIN.avPlayerController.view.addSubview(self.menuViewController.view)
            self.menuViewController.scoreUpdate(match: actualMatchObj!, teamLogoUrl: (viewConfiguration?.images.getValue(key: "teamLogo"))!)
            self.menuViewController.showPanel()
            
            //opacityOverlay.frame = UIScreen.main.bounds
            resetMenu()
            
            
        }
        else if(videoPlayer_SECONDARY.PlayerRate() != 0 && viewMultiangleCameraIsActive){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleShow()
                
            }, completion: nil)
            
        }
        
        
        //super.touchesBegan(touches, withEvent:event)
        self.next?.touchesBegan(touches, with: event)
    }
    
    
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touchEnd")
        
        if (canCloseTheView && menuViewController.view.alpha != 0) {
            resetMenu()
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.menuViewController.view.alpha = 0
            }, completion: {
                finish in
                
                self.menuViewController.view.removeFromSuperview()
                if(self.isMenuOn){
                    self.isMenuOn = false
                    self.videoPlayer_MAIN.InteractionStart()
                    self.videoPlayer_MAIN.avPlayerController.view.frame = self.initView.bounds
                    self.videoPlayer_MAIN.avPlayerController.view.layer.frame = self.initView.bounds
                }
            })
        }
        else if(viewMultiangleCameraIsActive && self.multicamPanel.panelMultiAngleMenu.alpha != 0){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleRemoveDelayed()
                self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
            }, completion: nil)
            
        }
        self.next?.touchesEnded(touches, with: event)
    }
    
    
    
    public override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touchCancelled")
        
        if (canCloseTheView && menuViewController.view.alpha != 0) {
            resetMenu()
            
            UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.menuViewController.view.alpha = 0
            }, completion: {
                finish in
                self.menuViewController.view.removeFromSuperview()
                if(self.isMenuOn){
                    self.isMenuOn = false
                    self.videoPlayer_MAIN.InteractionStart()
                    self.videoPlayer_MAIN.avPlayerController.view.frame = self.initView.bounds
                    self.videoPlayer_MAIN.avPlayerController.view.layer.frame = self.initView.bounds
                }
            })
        }
        else if(viewMultiangleCameraIsActive && self.multicamPanel.panelMultiAngleMenu.alpha != 0){
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.multicamPanel.panelMultiAngleRemoveDelayed()
                self.multicamPanel.panelMultiAngleMenu.layer.zPosition = 1000
            }, completion: nil)
            
        }
        
        self.next?.touchesCancelled(touches, with: event)
    }
    
    public override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("touchMoved")
        
        if (canCloseTheView && videoPlayer_MAIN.PlayerRate() != 0 && !viewOtherMatchesIsActive && !viewNotificationsIsActive && !viewMultiangleCameraIsActive && !viewLineupsIsActive) {
            
            self.isMenuOn = true
            self.videoPlayer_MAIN.InteractionStop()
            
            ////NSLog("touchMoved --> OK")
            //var center = false
            var up = false
            var right = false
            var down = false
            var left = false
            
            let touch:UITouch = touches.first!
            let newLocation = touch.location(in: self.view)
            let prevLocation = touch.previousLocation(in: self.view)
            
            if (newLocation.x - prevLocation.x > touchToleranceLight) {
                //NSLog("touchMove -> right")
                right = true
            } else if (prevLocation.x - newLocation.x > touchToleranceLight) {
                //NSLog("touchMove -> left")
                left = true
            } else if (newLocation.x - touchCheckpointX > touchToleranceMax) {
                //NSLog("touchMoveForced -> right")
                right = true
            } else if (touchCheckpointX - newLocation.x > touchToleranceMax) {
                //NSLog("touchMoveForced -> left")
                left = true
            } else if (newLocation.y - prevLocation.y > touchToleranceLight) {
                //NSLog("touchMove -> down")
                down = true
            } else if (prevLocation.y - newLocation.y > touchToleranceLight) {
                //NSLog("touchMove -> top")
                up = true
            } else if (newLocation.y - touchCheckpointY > touchToleranceMax) {
                //NSLog("touchMoveForced -> down")
                down = true
            } else if (touchCheckpointY - newLocation.y > touchToleranceMax) {
                //NSLog("touchMoveForced -> top")
                up = true
            }
            
            if (menuOtherMatchesIsActive) {
                up = false
            }
            if (menuNotificationsIsActive) {
                right = false
            }
            if (menuMultiangleCameraIsActive) {
                down = false
            }
            if (menuLineupsIsActive) {
                left = false
            }
            
            if (up || right || down || left) {
                //NSLog("touchMove --> new Direction --> up \(up) right \(right) down \(down) left \(left) ")
                
                resetMenu()
                
                if (up) {
                    menuViewController.showMenuUp()
                    
                    menuOtherMatchesIsActive = true
                } else if (down) {
                    menuViewController.showMenuDown()
                    menuMultiangleCameraIsActive = true
                } else if (right) {
                    menuViewController.showMenuRight()
                    menuNotificationsIsActive = true
                } else if (left) {
                    menuViewController.showMenuLeft()
                    menuLineupsIsActive = true
                }
                
                touchCheckpointX = newLocation.x
                touchCheckpointY = newLocation.y
            }
        }
        
        self.next?.touchesMoved(touches, with: event)
    }
    
    
    func resetMenu() {
        //NSLog("resetMenu")
        
        menuViewController.hideMenu()
        
        menuOtherMatchesIsActive = false
        menuNotificationsIsActive = false
        menuMultiangleCameraIsActive = false
        menuLineupsIsActive = false
    }
    
    
    func viewOtherMatches() {
        viewOtherMatchesIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundOtherMatchesImage)
        
        let oldFrame = self.videoPlayer_MAIN.avPlayerController.view.frame
        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x: 0.5, y: 0.9)
        self.videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x: 0.92,y: 0.5)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.66, y: 0.66)
            
            self.initView.viewWithTag(2000)?.isHidden = false
        }, completion: nil)
        
        //dispatch_async(dispatch_get_global_queue(DispatchQueue.GlobalQueuePriority., 0)) {
        
        DispatchQueue.main.async(){
            self.createTodayMatchesPanel()
            self.videoPlayer_MAIN.avPlayerController.view.isUserInteractionEnabled = false
            self.todayMatchesPanel.view.isUserInteractionEnabled = true
        }
    }
    
    func createTodayMatchesPanel(){
        todayMatchesPanel = TodayMatchesPanel(parameters: parameters, teamLogoBaseUrl: viewConfiguration!.images.getValue(key: "teamLogo")!, videoDataUrl: viewConfiguration!.feeds["feedVideodata"]!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        todayMatchesPanel.delegate = self
        todayMatchesPanel.view.alpha = 1
        todayMatchesPanel.actualDate = actualMatchObj?.DateCET
        DispatchQueue.main.async() {
            self.initView.addSubview(self.todayMatchesPanel.view)
        }
    }
    
    
    
    func viewNotifications() {
        viewNotificationsIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundNotificationsImage)
        
        let oldFrame = self.videoPlayer_MAIN.avPlayerController.view.frame
        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x: 0.1, y: 0.5)
        self.videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x: 0.1, y: 0.5)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.67, y: 0.67)
            
            self.initView.viewWithTag(3000)?.isHidden = false
        }, completion: nil)
        
        DispatchQueue.main.async {
            self.createNotificationPanel()
            self.notificationPanel.view.isUserInteractionEnabled = true
        }
    }
    
    
    func createNotificationPanel(){
        notificationPanel = NotificationPanel(parameters: parameters, eventsIconsBaseUrl: viewConfiguration!.images.getValue(key: "events_icons_commentary")!, isLive: actualMatchObj!.isLive(), skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        notificationPanel.delegate = self
        notificationPanel.videoData = self.videoData
        notificationPanel.view.alpha = 1
        DispatchQueue.main.async() {
            self.initView.addSubview(self.notificationPanel.view)
        }
    }
    
    func viewMultiangleCamera() {
        viewMultiangleCameraIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundMulticamImage)
        
        let oldFrame = self.videoPlayer_MAIN.avPlayerController.view.frame
        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x:0.5, y:0.1)
        self.videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = CGRect(x: 0,y: 0, width: self.screenRect.width, height: self.screenRect.height)
        mainPlayerAnchorPointStore = CGPoint(x:0.5, y:0.1)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.65, y: 0.65)
        }, completion: nil)
        
        DispatchQueue.main.async {
            self.createMulticamPanel()
            
        }
    }
    
    func createMulticamPanel(){
        
        
        
        var warningLabel = ""
        if let val = viewConfiguration?.labels.getValue(key: "multicamWarning"){
            if(val.characters.count>0){
                warningLabel = val
            }
        }
        
        multicamPanel = MulticamPanel(parameters: parameters, isLive: actualMatchObj!.isLive(), urlEventsIcons: viewConfiguration!.images.getValue(key: "events_icons_multicam")!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "", warning: warningLabel)
        
        
        multicamPanel.delegate = self
        multicamPanel.view.alpha = 1
        
        DispatchQueue.main.async() {
            self.initView.addSubview(self.multicamPanel.view)
            self.multicamPanel.view.isUserInteractionEnabled = true
        }
    }
    
    
    
    func playMultiangleCameraAvPlayerAtIndex(cameraid: String) {
        
        if (videoPlayer_SECONDARY.PlayerRate() != 0) {
            videoPlayer_SECONDARY.Pause()
            videoPlayer_SECONDARY.Seek(time: kCMTimeZero)
        }
        selectedCameraCurrent = cameraid
        //        videoPlayer_SECONDARY.avPlayerController.removeAllItems()
        if(dictMultiangleCameraAvPlayerQueue.count>0){
            if(dictMultiangleCameraAvPlayerQueue[String(cameraid)] != nil){
                videoPlayer_SECONDARY.SetVideo(item: dictMultiangleCameraAvPlayerQueue[String(cameraid)]!)
                videoPlayer_SECONDARY.Play()
            }
        }
        
        
    }
    
    
    func viewLineups() {
        viewLineupsIsActive = true
        canCloseTheView = false
        menuViewController.view.alpha = 0
        self.multicamPanel.panelMultiAngleMenu.alpha = 0
        initView.backgroundColor = UIColor(patternImage: backgroundLineupsImage)
        //initView.backgroundColor = UIColor.blueColor()
        
        let oldFrame = self.videoPlayer_MAIN.avPlayerController.view.frame
        self.videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x:0.92,y: 0.5)
        self.videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        //SAVE ACTUAL STATE
        mainPlayerFrameStore = self.videoPlayer_MAIN.avPlayerController.view.frame
        mainPlayerAnchorPointStore = CGPoint(x:0.92, y:0.5)
        
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.52, y: 0.52)
            
            self.initView.viewWithTag(1000)?.isHidden = false
            //self.spinnerLineups.startAnimating()
            
            
        }, completion: nil)
        
        
        
        DispatchQueue.main.async
        {
                self.createLineupsPanel()
        }
    }
    
    
    func createLineupsPanel(){
        lineupsPanel = LineupsPanel(parameters: parameters, urlEventsIcons: viewConfiguration!.images.getValue(key: "events_icons_lineup")!, skin: (self.viewConfiguration!.parameters["skin"] != nil) ? self.viewConfiguration!.parameters["skin"]! : "")
        self.lineupsPanel.view.alpha = 1
        self.lineupsPanel.view.isUserInteractionEnabled = true
        
        DispatchQueue.main.async() {
            self.addChildViewController(self.lineupsPanel)
            self.initView.addSubview(self.lineupsPanel.view)

        }
    }
    
    //////////////////////////////////////////////
    //////////// COMMON FUNCTIONS ////////////////   <--- <3
    //////////////////////////////////////////////
    
    
    
    func goToMatch(videoData:VideoDataModel, matchData: Match){
        
        DataProvider.sharedInstance.dismissMatchData()
        
        self.videoUrl = videoData.videoUrl
        self.matchId = Int(videoData.eventId)!
        self.viewConfiguration = viewConfiguration
        self.currentVideoId = videoData.videoId
        backActions()
        
        removePlayers()
        
        
        
        viewDidLoad()
        viewDidAppear(true)
        
        videoPlayer_MAIN.Show()
        videoPlayer_MAIN.Play()
        //
        //        //UPDATE VIEW
        //        InitData()
        //        InitAvPlayer()
        
        
        
        //DataProvider.sharedInstance.dismissMatchData()
        //navigationController?.popViewControllerAnimated(false)
        
        //Match.playMatch(videoData, matchId: matchData.MatchId, viewConfiguration: viewConfiguration!)
        
    }
    
    
    func createPictureInPictureView(){
        
        var oldFrame = videoPlayer_MAIN.avPlayerController.view.frame
        videoPlayer_MAIN.avPlayerController.view.layer.anchorPoint = CGPoint(x:0,y: 0)
        videoPlayer_MAIN.avPlayerController.view.frame = oldFrame
        
        oldFrame = videoPlayer_SECONDARY.avPlayerController.view.frame
        videoPlayer_SECONDARY.avPlayerController.view.layer.anchorPoint = CGPoint(x:0,y: 0)
        videoPlayer_SECONDARY.avPlayerController.view.frame = oldFrame
        videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
        self.addChildViewController(videoPlayer_SECONDARY.avPlayerController)
        initView.addSubview(videoPlayer_SECONDARY.avPlayerController.view)
        
        mainPlayerFrameStore = videoPlayer_MAIN.avPlayerController.view.frame
        mainPlayerAnchorPointStore = CGPoint(x:0,y: 0)
        
        secondaryPlayerFrameStore = videoPlayer_SECONDARY.avPlayerController.view.frame
        secondaryPlayerAnchorPointStore = CGPoint(x:0,y: 0)
        
        avPlayerTempWidth = screenRect.width // self.videoPlayer_MAIN.avPlayerController.view.frame.width
        self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 1000
        
        UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.x = 50
            self.videoPlayer_MAIN.avPlayerController.view.frame.origin.y = 50
            
            if(!self.actualMatchObj!.isLive() && self.viewConfiguration!.parameters["IS_DEMO_MODE"]! == "false"){
                self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.0000001, y: 0.0000001)
            }else{
                self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.35, y: 0.35)
                
            }
        }, completion: {
            (Value: Bool) in
            
            self.videoPlayer_MAIN.avPlayerController.view.isHidden = false
        })
        
        self.videoPlayer_SECONDARY.avPlayerController.view.frame.origin.x = 0
        self.videoPlayer_SECONDARY.avPlayerController.view.frame.origin.y = 0
        self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0.2, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 1
            
            self.videoPlayer_SECONDARY.avPlayerController.view.transform = self.videoPlayer_SECONDARY.avPlayerController.view.transform.scaledBy(x: self.avPlayerTempWidth / self.videoPlayer_SECONDARY.avPlayerController.view.frame.width, y: self.avPlayerTempWidth / self.videoPlayer_SECONDARY.avPlayerController.view.frame.width)
            self.videoPlayer_SECONDARY.Play()
            self.videoPlayer_SECONDARY.Mute()
            
            
        }, completion: {
            (Value: Bool) in
        })
        
        
    }
    
    func createTwoLiveView(transform: Bool = true){
        self.videoPlayer_SECONDARY.avPlayerController.view.layer.anchorPoint = CGPoint(x:0, y:0)
        self.videoPlayer_SECONDARY.avPlayerController.view.frame = CGRect(x: 980, y: 420, width: 850, height: 531)
        self.videoPlayer_SECONDARY.avPlayerController.view.layer.frame = CGRect(x: 980, y: 420, width: 850, height: 531)
        self.videoPlayer_SECONDARY.avPlayerController.view.layer.shadowOpacity = 0
        avPlayerTempWidth = screenRect.width // self.videoPlayer_MAIN.avPlayerController.view.frame.width
        
//        self.addChildViewController(videoPlayer_SECONDARY.avPlayerController)
//        initView.addSubview(videoPlayer_SECONDARY.avPlayerController.view)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            if(transform){
                self.videoPlayer_MAIN.avPlayerController.view.transform = self.videoPlayer_MAIN.avPlayerController.view.transform.scaledBy(x: 0.5, y: 0.5)
                self.videoPlayer_MAIN.avPlayerController.view.frame = CGRect(x: 90, y: 420, width: 850, height: 531)
                self.videoPlayer_MAIN.avPlayerController.view.layer.frame = CGRect(x: 90, y: 420, width: 850, height: 531)
            }
            
        }, completion: {
            (Value: Bool) in
            //self.videoPlayer_MAIN.avPlayerController.view.frame = CGRect(x: 90, y: 420, width: 850, height: 531)
            self.videoPlayer_MAIN.avPlayerController.view.isHidden = false
            self.videoPlayer_MAIN.avPlayerController.view.layer.zPosition = 1000
            self.videoPlayer_SECONDARY.avPlayerController.view.isHidden = false
            
            UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.videoPlayer_SECONDARY.avPlayerController.view.transform = self.videoPlayer_SECONDARY.avPlayerController.view.transform.scaledBy(x: 1.0, y: 1.0)
                
                
            }, completion: {
                (Value: Bool) in
                self.videoPlayer_SECONDARY.avPlayerController.view.frame = CGRect(x: 980, y: 420, width: 850, height: 531)
                self.videoPlayer_SECONDARY.avPlayerController.view.layer.frame = CGRect(x: 980, y: 420, width: 850, height: 531)
                
                self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 1
                //self.videoPlayer_SECONDARY.avPlayerController.view.backgroundColor = UIColor.black
            })
            
        })
    }
    
    func createPlaylistAvPlayer_SECONDARY( dictQueue: inout Dictionary<String, AVPlayerItem>, ID: String, url: String, isFirst: Bool){
        
        //TOKENIZE URL
        
        let urlTokenized: String = AkamaiTokenizer.tokenize(url: url, secret: secret)
        
        let url = NSURL(string: urlTokenized)
        let avAsset = AVAsset(url: url! as URL)
        let avPlayerItem = AVPlayerItem(asset: avAsset)
        
        dictQueue[ID] = avPlayerItem
        if(isFirst){
            selectedCameraCurrent = ID
            self.videoPlayer_SECONDARY.SetVideo(item: avPlayerItem)
            //self.videoPlayer_SECONDARY.Play()
        }
    }
    
    func removePlayers(){
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.videoPlayer_MAIN.Pause()
            self.videoPlayer_SECONDARY.Pause()
            
            self.videoPlayer_MAIN.avPlayerController.view.alpha = 0
            self.videoPlayer_SECONDARY.avPlayerController.view.alpha = 0
            
            //            self.videoPlayer_MAIN.avPlayerController.removeAllItems()
            //self.videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
            
            //            self.videoPlayer_SECONDARY.avPlayerController.removeAllItems()
            //self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            self.videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
            self.videoPlayer_MAIN.avPlayerController.view.removeFromSuperview()
            self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
        })
        
        
    }
    
    func drawMatchName(match: Match, origin: CGPoint){
                
        let homeTeamLabel = UILabel()
        let awayTeamLabel = UILabel()
        
        homeTeamLabel.text = match.HomeTeamName.uppercased()
        awayTeamLabel.text = match.AwayTeamName.uppercased()
        
        let imgHome = Utility.GetCachedImage(key: "lineup_team_" + String(match.HomeTeamId), url: (viewConfiguration?.images.getValue(key: "teamLogo")?.replacingOccurrences(of: "{TEAMID}", with: String(match.HomeTeamId)))!)
        
        let imgAway = Utility.GetCachedImage(key: "lineup_team_" + String(match.AwayTeamId), url: (viewConfiguration?.images.getValue(key: "teamLogo")?.replacingOccurrences(of: "{TEAMID}", with: String(match.AwayTeamId)))!)
        
        let homeTeamImage = UIImageView()
        let awayTeamImage = UIImageView()
        
        homeTeamImage.image = imgHome
        awayTeamImage.image = imgAway
        
    }
    
    func multiangleRemove(){
        //multiangleCameraSelectedScrollView.alpha = 0
        self.dictMultiangleCameraAvPlayerQueue = [:]
    }
    
    // MARK: -
    
    // MARK: Panel delegates
    func eventSelected(event: EventModel) {
        
        isPictureInPicture = true
        var videoId = ""
        if(actualMatchObj!.isLive()){
            videoId = actualMatchObj!.Videos.Live[0].VideoId
        }else if(actualMatchObj!.isScheduled()){
            videoId = actualMatchObj!.Videos.Scheduled[0].VideoId
        }else if(actualMatchObj!.isReplay()){
            videoId = actualMatchObj!.Videos.Replay[0].VideoId
        }
        
        
        if(videoId.count>0){
            //videoData = VideoDataController().getVideoData(feedVideoData: viewConfiguration!.feeds["feedVideodata"]!, videoId: String(videoId))!
            if (videoData.videoUrl.isEmpty == false) {
                matchId = Int(videoData.eventId)!
                videoUrl = videoData.videoUrl
                seekTime = ""
                videoStartTime = videoData.timeCodeIn
                if(videoStartTime.characters.count>0 && String(describing: event.timeUTC).characters.count>0){
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    
                    
                    let timecode = String(describing: event.timeUTC)
                    
                    let dateStringFormatter = DateFormatter()
                    dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
                    dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +SSSS"
                    dateStringFormatter.timeZone = NSTimeZone.init(abbreviation: "UTC")! as TimeZone
                    
                    let eventTimeDate = dateStringFormatter.date(from: timecode)
                    
                    let startTimeDate = dateFormatter.date(from: (videoStartTime as NSString).substring(to: 19))
                    
                    let userCalendar = Calendar(identifier: .gregorian)
                    let hourMinuteComponents = Set<Calendar.Component>([.hour, .minute, .second])
                    let timeDifference = userCalendar.dateComponents(
                        hourMinuteComponents,
                        from: startTimeDate!,
                        to: eventTimeDate!)
                    
                    let diff = (timeDifference.hour! * 3600) + (timeDifference.minute! * 60) + timeDifference.second!
                    
                    var time = CMTimeMakeWithSeconds(1,1)
                    
                    var _dictMultiangleCameraAvPlayerQueue: Dictionary<String, AVPlayerItem> = [:]
                    
                    
                    if(self.videoPlayer_SECONDARY.avPlayerController.parent == nil){
                        self.videoPlayer_SECONDARY.Initialize(parentViewController: self, parentView: initView, enableControls: true)
                    }
                    
//                    if (self.videoPlayer_SECONDARY.PlayerRate() != 0) {
//                        self.videoPlayer_SECONDARY.Pause()
//                        self.videoPlayer_SECONDARY.Seek(time: kCMTimeZero)
//                    }
                    
                    createPlaylistAvPlayer_SECONDARY(dictQueue: &_dictMultiangleCameraAvPlayerQueue, ID: String(describing: event.id), url: videoData.videoUrl, isFirst: true)
                    
                    
                    if(videoPlayer_MAIN.PlaybackType() == "LIVE"){
                        time = CMTimeMakeWithSeconds(Double(diff-10), 1)
                    }else{
                        time = CMTimeMakeWithSeconds(Double(diff-10), 1)    //videoPlayer_MAIN.Duration().timescale)
                    }
                    
                    notificationPanel.view.alpha = 0
                    
                    createPictureInPictureView()
                    
                    //self.videoPlayer_SECONDARY.avPlayerController.view.isUserInteractionEnabled = true
                    
//                    DispatchQueue.main.async {
//                        self.videoPlayer_SECONDARY.ShowControls = false
//                        self.videoPlayer_SECONDARY.Seek(time: time)
//                    }
                    
                    self.videoPlayer_SECONDARY.ShowControls = false
                    self.videoPlayer_SECONDARY.Seek(time: time)
                    
                    
                }
                
                
            }
            
        }
    }
    
    func matchSelected(videoId: String, videoUrl: String, transform: Bool, newMatch: Match)
    {
        var _dictMultiangleCameraAvPlayerQueue = Dictionary<String, AVPlayerItem>()
        self.videoPlayer_SECONDARY.Pause()
        
        if(self.videoPlayer_SECONDARY.avPlayerController.parent == nil){
            self.videoPlayer_SECONDARY.Initialize(parentViewController: self, parentView: initView, enableControls: false)
        }
        
        self.videoPlayer_SECONDARY.SetVideo(url: videoUrl)
        
        createTwoLiveView(transform: transform)
        self.videoPlayer_SECONDARY.avPlayerController.view.layer.isHidden = false
        self.videoPlayer_SECONDARY.Play()
        self.videoPlayer_SECONDARY.Mute()
        
    }
    
    func goToNewMatch(newMatch: Match, videoData: VideoDataModel){
        videoPlayer_MAIN.Pause()
        videoPlayer_SECONDARY.Pause()

        goToMatch(videoData: videoData, matchData: newMatch)
        
    }
    
    func multicamSelected(cameras: ClipModel){
        
        viewMultiangleCameraHighlightedIsActive = true
        
        var i = 0;
        for item:VideoClipModel in cameras.Videos {
             if(item.UrlMP4 != nil){
                createPlaylistAvPlayer_SECONDARY(dictQueue: &dictMultiangleCameraAvPlayerQueue, ID: item.ID!, url: item.UrlMP4!, isFirst: i==0)
                
            }else if(item.UrlHLS != nil){
                createPlaylistAvPlayer_SECONDARY(dictQueue: &dictMultiangleCameraAvPlayerQueue, ID: item.ID!, url: item.UrlHLS!, isFirst: i==0)
                
            }else{
                return
            }
            dictMultiangleCameraOrder[String(i)] = item
            
            i += 1;
        }
        
        createPictureInPictureView()
        self.videoPlayer_SECONDARY.Play()
    }
    
    func multiangleSelected(cameraid: String){
        
        self.addChildViewController(videoPlayer_SECONDARY.avPlayerController)
        initView.addSubview(videoPlayer_SECONDARY.avPlayerController.view)
        
        playMultiangleCameraAvPlayerAtIndex(cameraid: cameraid)
        self.videoPlayer_SECONDARY.Play()
    }
    
    func goBack(){
        self.goBackToMulticam()
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        
        DataProvider.sharedInstance.dismissMatchData()
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.videoPlayer_MAIN.Hide()
            self.videoPlayer_SECONDARY.Hide()
            
            
        }, completion: {
            (Value: Bool) in
            //                self.videoPlayer_MAIN.Dismiss()
            //                self.videoPlayer_SECONDARY.Dismiss()
            self.videoPlayer_MAIN.Pause()
            self.videoPlayer_SECONDARY.Pause()
            self.videoPlayer_MAIN.avPlayerController.removeFromParentViewController()
            self.videoPlayer_MAIN.avPlayerController.view.removeFromSuperview()
            self.videoPlayer_SECONDARY.avPlayerController.removeFromParentViewController()
            self.videoPlayer_SECONDARY.avPlayerController.view.removeFromSuperview()
            Global.shared.isRefresh = true
            NotificationCenter.default.removeObserver(self)
            //self.navigationController?.popViewController(animated: true)
            //self.dismiss(animated: true, completion: nil)
            //FormSheetRouterAction.close(source: self)
        })
        
        super.dismiss(animated: true, completion: nil)
    }
    
}
