//
//  ViewPlayerSimple.swift
//  AppleTV
//
//  Created by Corrado Amatore on 01/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

public class ViewPlayerSimple: BaseViewController {
    
    public var VideoUrl: String  = ""
    let screenRect = UIScreen.main.bounds
    let videoPlayer = NGSVideoPlayer()
    
    override public func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 1)
        self.view.frame = screenRect
        self.view.isUserInteractionEnabled = true
        
        let backRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.backRecognizer(recognizer:)))
        self.view.addGestureRecognizer(backRecognizer)
        backRecognizer.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        videoPlayer.Initialize(parentViewController: self, parentView: self.view, enableControls: true)
        videoPlayer.SetVideo(url: VideoUrl)
        videoPlayer.Play()
    }
    
    deinit {
        NSLog("deinit")
    }
    
    @objc func backRecognizer(recognizer: UITapGestureRecognizer) {
        videoPlayer.Pause()
        self.dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
    
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
