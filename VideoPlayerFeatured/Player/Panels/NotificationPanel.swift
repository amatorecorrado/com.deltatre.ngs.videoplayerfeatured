//
//  NotificationPartialView.swift
//  AppleTV
//
//  Created by Corrado Amatore on 02/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

protocol NotificationPanelDelegate{
    func eventSelected(event: EventModel)
}

class NotificationPanel: BaseViewController {
    
    var delegate: NotificationPanelDelegate?
    
    let screenRect = UIScreen.main.bounds
    //-------  VARIABLES
    var notificationList: [EventModel] = []
    var actualMatchObj: Match?
    var videoData = VideoDataModel()
    var translationData: Translations?
    
    var eventsIconsBaseUrl: String?
    var feedVideodataUrl: String?
    var feedEventsNGS: String?
    var videoStartTime: String = ""
    var notificationTimeCodeStart = ""
    var videoUrl: String  = ""
    var seekTime: String = ""
    var counterPenalty = 1
    var notificationOffsetHeight = 0
    var matchId: Int = 0
    var notificationItemHeight: CGFloat = 350
    var isPictureInPicture = false
    var isLive = true
    var skin: String = ""
    
    var cup:Int = 0
    var season:Int = 0
    var roundId: Int = 0
    var matchDay: Int = 0
    
    var timerNotifications: Timer? = Timer.init()
    var parameters = ParametersModel()
    
    //-------  UI VIEWS
    var notificationsScrollView = UIScrollView()
    let spinnerNotifications = UIActivityIndicatorView(activityIndicatorStyle: .white)
    let warningLabel = UILabel()
    
    
    //-------  CONSTRUCTORS
    /*
     init(eventsIconsBaseUrl: String, feedVideodataUrl: String){
     super.init(nibName: nil, bundle: nil)
     self.eventsIconsBaseUrl = eventsIconsBaseUrl
     self.feedVideodataUrl = feedVideodataUrl
     }
     
     init(feedEventsNGS: String, feedVideodataUrl: String, eventsIconsBaseUrl: String, isLive:Bool, translationData: Translations?){
     super.init(nibName: nil, bundle: nil)
     self.eventsIconsBaseUrl = eventsIconsBaseUrl
     self.feedVideodataUrl = feedVideodataUrl
     self.feedEventsNGS = feedEventsNGS
     self.translationData = translationData
     }
     */
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    init(parameters: ParametersModel, eventsIconsBaseUrl: String, isLive: Bool, skin: String){
        super.init(nibName: nil, bundle: nil)
        self.parameters = parameters
        self.cup = Int(parameters.Cup!)!
        self.season = Int(parameters.Season!)!
        self.roundId = Int(parameters.RoundId!)!
        self.matchDay = Int(parameters.Matchday!)!
        self.matchId = Int(parameters.MatchId!)!
        self.eventsIconsBaseUrl = eventsIconsBaseUrl
        self.isLive = isLive
        self.skin = skin;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //-------  EVENTS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinnerNotifications.frame = CGRect(x: 1650, y: self.screenRect.height / 2 - 20, width: 40, height: 40)
        
        DispatchQueue.main.async() {
            self.view.addSubview(self.spinnerNotifications)
            self.spinnerNotifications.startAnimating()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationPanel.handleNotification(notification:)), name: NSNotification.Name(rawValue: "notificationsDataReady"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        notificationList = DataProvider.sharedInstance.getMatchNotifications(parameters: parameters)
//        if(isLive){
//            notificationList = notificationList.reversed()
//        }
        
        //notificationList = notificationList.reversed()
        
        DispatchQueue.main.async() {
            self.drawNotifications()
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)?) {
        NSLog("Dismiss NotificationPartialView animated")
        
        Utility.timerStop(timer: &timerNotifications)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notificationsDataReady"), object: nil)
    }
    
    func drawNotifications(){
        counterPenalty = 1
        for item in notificationsScrollView.subviews {
            item.removeFromSuperview()
        }
        
        warningLabel.frame = CGRect(x: 1500, y: self.screenRect.height / 2 - 20, width: 500, height: 40)
        warningLabel.font = UIFont.init(name: "Dosis-Light", size: 28)
        warningLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
        warningLabel.text = String("Notifications coming soon.").uppercased()
        
        if(self.notificationList.count>0){
            
            notificationOffsetHeight = 0
            
            for playByPlayObj:EventModel in self.notificationList {
                
                if(playByPlayObj.eventType == "StartFirstHalf"){
                    notificationTimeCodeStart = String(describing: playByPlayObj.timeUTC)
                }
                if (playByPlayObj.eventType != "Foul" && playByPlayObj.eventType != "Skill" && playByPlayObj.eventType != "FreeKick" && playByPlayObj.eventType != "Corner") {
                    drawNotification(playByPlay: playByPlayObj, isAdd: false, isUpdate: false, view: UIView())
                }
            }
            
            
            notificationsScrollView.frame = CGRect(x: 1450, y: 0, width: 430, height: screenRect.height)
            notificationsScrollView.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            notificationsScrollView.contentSize = CGSize(width: 430, height: notificationOffsetHeight + 20)
            notificationsScrollView.autoresizingMask = UIViewAutoresizing.flexibleHeight
            notificationsScrollView.isScrollEnabled = true
            notificationsScrollView.clipsToBounds = false
            
            self.notificationsScrollView.showsVerticalScrollIndicator = false
            self.notificationsScrollView.showsHorizontalScrollIndicator = false
            
            
            self.view.addSubview(notificationsScrollView)
            
            UserExperienceUtility.animateSubviewsHorizontally(view: notificationsScrollView, dimension: 2000, duration: 0.5)
            
            if (spinnerNotifications.isAnimating) {
                spinnerNotifications.stopAnimating()
            }
            
        }else{
            self.view.addSubview(warningLabel)
            
            if (spinnerNotifications.isAnimating) {
                spinnerNotifications.stopAnimating()
            }
            
            return
        }
    }
    
    func drawNotification(playByPlay: EventModel, isAdd: Bool, isUpdate: Bool, view: UIView) {
    
        let haveThumb = false
        var imgView = UIImageView()
        
        let buttonHeight =  140
        
        var buttonFrame = CGRect(x: 0, y: notificationOffsetHeight + 20, width: 430, height: buttonHeight)
        if (isUpdate || isAdd) {
            buttonFrame.origin.x = 0
            buttonFrame.origin.y = view.frame.origin.y
        }
        
        
        let timeFrame = CGRect(x: 10, y: 20, width: 100, height: 50)
        let titleFrame = CGRect(x: 120, y: 25, width: 300, height: 200)
        let verticalBarFrame = CGRect(x: 105, y: 10, width: 2, height: 60)
        let verticalBarGoalFrame = CGRect(x: 0, y: 0, width: 4, height: 140)
        
        let iconFrame = CGRect(x: 20, y:  80, width: 70, height: 50)
        
        let button = CustomButton()
        button.tag = Int(playByPlay.id!)
        button.frame = buttonFrame
        //button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.backgroundColorSelected(SelectedColor: UIColor.white,Alpha: 0.1)
        
        var labelColor = UIColor.white //UIColor.init(red: 140.0/255.0, green: 183.0/255.0, blue: 222.0/255.0, alpha: 1)
        
        if (skin == "light") {
            labelColor = UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1)
        } else if (skin == "white") {
            labelColor = UIColor.white
        }
        
        if(playByPlay.eventType == "Goal" || playByPlay.eventType == "OwnGoal" || playByPlay.eventType == "GoalOnPenalty"){
            
            let verticalBarGoalLabel = UILabel(frame: verticalBarGoalFrame)
            
            verticalBarGoalLabel.textColor = UIColor.init(red: 140.0/255.0, green: 183.0/255.0, blue: 222.0/255.0, alpha: 1)
            verticalBarGoalLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            verticalBarGoalLabel.numberOfLines = 0
            verticalBarGoalLabel.textAlignment = NSTextAlignment.center
            
            //eventLabel.textColor = UIColor.init(red: 0.0/255.0, green: 170.0/255.0, blue: 220.0/255.0, alpha: 1)
            let leftBorder = CALayer()
            //rightBorder.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.2).CGColor
            leftBorder.backgroundColor = (skin == "light") ? UIColor.init(red: 255.0/255.0, green: 126.0/255.0, blue: 0.0/255.0, alpha: 1).cgColor : UIColor.init(red: 0.0/255.0, green: 170.0/255.0, blue: 220.0/255.0 , alpha: 1).cgColor
            leftBorder.frame = CGRect(x: 0,y: 0, width: 4, height: 140)             //  numberLabel.frame.width-5, 15, 1, 30)
            verticalBarGoalLabel.layer.addSublayer(leftBorder)
            button.addSubview(verticalBarGoalLabel)
            
        }
        //button.adjustsImageWhenHighlighted = true
        
        button.addTarget(self, action: #selector(NotificationPanel.handleNotificationButton(sender:)), for: UIControlEvents.allEvents)
        
        let gameTime = playByPlay.minute?.description
        //let gameTimeSplitted = gameTime.componentsSeparatedByString("+")
        
        let timeLabel = UILabel(frame: timeFrame)
        
        //PENALTIES
        if(playByPlay.phaseId == 5)
        {
            timeLabel.text = "P." + String(counterPenalty)
            counterPenalty += 1
        }
        else{
            timeLabel.text = gameTime
        }
        timeLabel.font = UIFont.init(name: "Dosis-Light", size: 60.0)
        timeLabel.textAlignment = NSTextAlignment.center
        timeLabel.textColor = labelColor
        timeLabel.lineBreakMode = NSLineBreakMode.byClipping
        button.addSubview(timeLabel)
        
        
        var evt = ""
        if let found = playByPlay.attributes?.contains("PenaltyShootOut"), found == true{
            if let wrong = playByPlay.attributes?.contains("Wrong"), wrong == true{
                evt = "PenaltyWrong"
            }else{
                evt = "PenaltyScored"
            }
        }
        else if(playByPlay.eventType?.lowercased() == "penalty"){
            if let wrong = playByPlay.attributes?.contains("Wrong"), wrong == true{
                evt = "PenaltyWrong"
            }
        }
        else if(playByPlay.eventType?.lowercased() == "goal"){
            if let wrong = playByPlay.attributes?.contains("Penalty"), wrong == true{
                evt = "PenaltyScored"
            }else{
                evt = playByPlay.eventType!
            }
        }else{
            evt = playByPlay.eventType!
        }
        
        if evt.characters.count>0, let baseurl = self.eventsIconsBaseUrl{
            
            let urlIcon: String = baseurl + evt + ".png"
            
            let img = Utility.GetCachedImage(key: "notification_events_" + String( playByPlay.id!), url: urlIcon)
            if(img != nil){
                //let iconViewImg = UIImageView(frame: iconFrame)
                imgView = UIImageView(image: Utility.newImageFromImage(image: img!, scaledToSize: CGSize(width: 70, height: 50), withAplha: 1))
                imgView.frame = iconFrame
                button.addSubview(imgView)
            }
        }
        
        
        if(timeLabel.text != nil && (timeLabel.text?.characters.count)!>0){
            let verticalBarLabel = UILabel(frame: verticalBarFrame)
            
            verticalBarLabel.textColor = (skin == "light") ? UIColor.init(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1) : UIColor.white
            verticalBarLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            verticalBarLabel.numberOfLines = 0
            verticalBarLabel.textAlignment = NSTextAlignment.center
            let rightBorder = CALayer()
            rightBorder.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
            rightBorder.frame = CGRect(x:2, y: 10, width: 2, height: 100)             //  numberLabel.frame.width-5, 15, 1, 30)
            verticalBarLabel.layer.addSublayer(rightBorder)
            button.addSubview(verticalBarLabel)
            
        }
        
        
        let eventLabel = UILabel(frame: titleFrame)
        
        eventLabel.text = playByPlay.description
        eventLabel.font = UIFont.init(name: "Dosis-Light", size: 24)
        eventLabel.textAlignment = NSTextAlignment.left
        eventLabel.textColor = labelColor
        eventLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        eventLabel.numberOfLines = 3
        eventLabel.sizeToFit()
        
        button.addSubview(eventLabel)
        
        if (isUpdate) {
            notificationsScrollView.insertSubview(button, belowSubview: view)
        } else {
            notificationsScrollView.addSubview(button)
        }
        
        if(haveThumb){
            notificationOffsetHeight += 350
        }else{
            notificationOffsetHeight += 140
        }
    }
    
    @objc func handleNotificationButton(sender: UIButton!) {
        NSLog("NotificationButton tag --> " + String(sender.tag))
        
        let eventId = sender.tag
        
        //        isPictureInPicture = true
        
        for item in notificationList {
            if (Int(item.id!) == eventId) {
                
                self.delegate!.eventSelected(event: item)
                break
                
            }
        }
    }
    
    func updateNotifications() {
        
        
        
        //---- REFRESH NOTIFICATIONS ------
        
        //if((self.viewNotificationsIsActive)  && self.notificationList.count>0){
        if(DataProvider.sharedInstance.oldNotifications.count>0){
            var previousNotificationView = UIView()
            for item in DataProvider.sharedInstance.newNotifications {
                var isPresent = false
                
                for itemOld in DataProvider.sharedInstance.oldNotifications {
                    if (item.id == itemOld.id) {
                        isPresent = true
                        
                        //item.code != itemOld.code ||
                        if (item.description != itemOld.description ||
                            item.minute?.description != itemOld.minute?.description) {
                            
                            //if (self.viewNotificationsIsActive) {
                            // update item notification
                            let view = self.notificationsScrollView.viewWithTag(Int(item.id!))
                            if(view != nil){
                                self.drawNotification(playByPlay: item, isAdd: false, isUpdate: true, view: view!)
                                view!.removeFromSuperview()
                            }
                            //}
                        }
                        
                    }
                }
                
                if (!isPresent) {
                    
                    //if (self.viewNotificationsIsActive) {
                    
                    // add item notification
                    
                    if (previousNotificationView.tag != 0) {
                        self.drawNotification(playByPlay: item, isAdd: false, isUpdate: true, view: previousNotificationView)
                    } else {
                        self.drawNotification(playByPlay: item, isAdd: true, isUpdate: false, view: UIView())
                    }
                    
                    if (self.notificationsScrollView.contentOffset.y < 700) {
                        self.notificationsScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                        self.setNeedsFocusUpdate()
                        self.updateFocusIfNeeded()
                    }
                    
                    //}
                }
                
                //if (self.viewNotificationsIsActive) {
                if let prev = self.notificationsScrollView.viewWithTag(Int(item.id!)){
                    previousNotificationView = prev
                }
                
                //}
            }
            
            for item in DataProvider.sharedInstance.oldNotifications {
                var isPresent = false
                
                for itemNew in DataProvider.sharedInstance.newNotifications {
                    if (item.id == itemNew.id) {
                        isPresent = true
                    }
                }
                
                if (!isPresent) {
                    //if (self.viewNotificationsIsActive) {
                    // remove item notification
                    let view = self.notificationsScrollView.viewWithTag(Int(item.id!))
                    if(view != nil){
                        view!.removeFromSuperview()
                    }
                    //}
                    
                }
            }
        }
        
        //if(self.notificationList.count==0 && _notificationList.count>0 && self.viewNotificationsIsActive)
        if(DataProvider.sharedInstance.oldNotifications.count==0 && DataProvider.sharedInstance.newNotifications.count>0){
            DataProvider.sharedInstance.oldNotifications = DataProvider.sharedInstance.newNotifications
            if (self.view.subviews.contains(self.warningLabel)){
                self.warningLabel.removeFromSuperview()
            }
            self.spinnerNotifications.startAnimating()
            self.drawNotifications()
        }else {
            DataProvider.sharedInstance.oldNotifications = DataProvider.sharedInstance.newNotifications
            
            //if (self.viewNotificationsIsActive && self.notificationList.count==0) {
            if (DataProvider.sharedInstance.oldNotifications.count==0) {
                self.view.addSubview(self.warningLabel)
            }
        }
        
        //CALCULATE SPACES SCROLLVIEWS
        
        //if(self.viewNotificationsIsActive){
        self.notificationOffsetHeight = 0
        for item in DataProvider.sharedInstance.oldNotifications {
            let view = self.notificationsScrollView.viewWithTag(item.id!)
            if(view != nil){
                view!.frame.origin.y = CGFloat(self.notificationOffsetHeight + 40)
                if (view!.viewWithTag(100) != nil) {
                    self.notificationOffsetHeight += Int(self.notificationItemHeight)
                } else {
                    self.notificationOffsetHeight += 140
                }
            }
        }
        self.notificationsScrollView.contentSize = CGSize(width: 380, height: CGFloat(self.notificationOffsetHeight + 20))
        //}
        
    }
    
    // MARK: -
    
    // MARK: Notifications data ready
    @objc func handleNotification(notification: NSNotification){
        if(notification.name.rawValue == "notificationsDataReady"){
            NSLog("UPDATE NOTIFICATIONS")
            DispatchQueue.main.async {
                self.updateNotifications()
            }
        }
    }
    
}
