//
//  PlayerMenuViewController.swift
//  AppleTV
//
//  Created by Corrado Amatore on 22/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit

class PlayerMenuViewController: BaseViewController {
    @IBOutlet weak var awayTeamLabel: UILabel!
    @IBOutlet weak var awayTeamImage: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var homeTeamImage: UIImageView!
    @IBOutlet weak var homeTeamLabel: UILabel!
    @IBOutlet weak var awayScorersStackView: UIStackView!
    @IBOutlet weak var homeScorersStackView: UIStackView!

    @IBOutlet weak var upMenuLabel: UILabel!
    @IBOutlet weak var upMenuImage: UIImageView!
    @IBOutlet weak var rightMenuLabel: UILabel!
    @IBOutlet weak var rightMenuImage: UIImageView!
    @IBOutlet weak var downMenuLabel: UILabel!
    @IBOutlet weak var downMenuImage: UIImageView!
    @IBOutlet weak var leftMenuImage: UIImageView!
    @IBOutlet weak var leftMenuLabel: UILabel!
    
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//    }
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//
//    }



    // MARK: - METHODS

    func showPanel(){
        self.view.alpha = 0
        
        hideMenu()
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.alpha = 1
            }, completion:nil)
    }
    
    func hidePanel(){
        self.view.alpha = 0
    }
    
    func showMenuUp(){
        showMenuItem(label: upMenuLabel,image: upMenuImage)
    }
    
    func showMenuDown(){
        showMenuItem(label: downMenuLabel,image: downMenuImage)
    }
    
    func showMenuRight(){
        showMenuItem(label: rightMenuLabel,image: rightMenuImage)
    }
    
    func showMenuLeft(){
        showMenuItem(label: leftMenuLabel,image: leftMenuImage)
    }
    
    func showMenuItem(label: UILabel!, image: UIImageView){
        label.alpha = 1
        image.alpha = 1
        label.transform = label.transform.scaledBy(x: 1.3, y: 1.3)
        image.transform = image.transform.scaledBy(x: 1.5, y: 1.5)
    }
    
    func hideMenu(){
        hideMenuItem(label: upMenuLabel,image: upMenuImage)
        hideMenuItem(label: downMenuLabel,image: downMenuImage)
        hideMenuItem(label: rightMenuLabel,image: rightMenuImage)
        hideMenuItem(label: leftMenuLabel,image: leftMenuImage)
    }
    
    func hideMenuItem(label: UILabel!, image: UIImageView){
        label.alpha = 0
        image.alpha = 0.5
        label.transform = CGAffineTransform.identity
        image.transform = CGAffineTransform.identity
        
    }
    
    public func scoreUpdate(match: Match, teamLogoUrl: String){
        
        if(self.homeScorersStackView != nil){
            for view in self.homeScorersStackView.subviews {
                view.removeFromSuperview()
            }
        }
        if(self.homeScorersStackView != nil){
            for view in self.awayScorersStackView.subviews {
                view.removeFromSuperview()
            }
        }
        
        
        homeTeamLabel.text = match.HomeTeamName.uppercased()
        awayTeamLabel.text = match.AwayTeamName.uppercased()
        
        let imgHome = Utility.GetCachedImage(key: "lineup_team_" + String(match.HomeTeamId), url: match.HomeTeamLogo)
        
        let imgAway = Utility.GetCachedImage(key: "lineup_team_" + String(match.AwayTeamId), url: match.AwayTeamLogo)
        
        homeTeamImage.image = imgHome
        awayTeamImage.image = imgAway
        
        // SCORE
        var scoreAway: String? = String(match.Results.AwayGoals)
        var scoreHome: String? = String(match.Results.HomeGoals)
        
        if (scoreHome == nil) {
            scoreHome = "0"
        }
        if (scoreAway == nil) {
            scoreAway = "0"
        }
        
        let score = scoreHome! + "-" + scoreAway!
        self.scoreLabel.text = score
        
        //SCORERS
        var index = 0
        if(match.Results.Scorers.HomeGoals.count>0){
            for item in match.Results.Scorers.HomeGoals {
                let offset = index * 30
                let homeScorersLabel = UILabel()
                homeScorersLabel.frame.origin = CGPoint(x: 0, y: offset)
                homeScorersLabel.frame.size = CGSize(width: homeScorersStackView.frame.width, height: 40)
                homeScorersLabel.textColor = UIColor.lightGray
                homeScorersLabel.font = UIFont(name: "Dosis-Regular", size: 25)
                homeScorersLabel.textAlignment = NSTextAlignment.right
                
                homeScorersLabel.text = item.PlayerWebName + " " + String(item.Minute)  + "'"
                
                homeScorersLabel.text = homeScorersLabel.text!.uppercased()
                homeScorersStackView.addSubview(homeScorersLabel)
                index = index + 1
            }
        }
        index = 0
        if(match.Results.Scorers.AwayGoals.count>0){
            for item in match.Results.Scorers.AwayGoals {
                let offset = index * 30
                let awayScorersLabel = UILabel()
                awayScorersLabel.frame.origin = CGPoint(x: 0, y: offset)
                awayScorersLabel.frame.size = CGSize(width: awayScorersStackView.frame.width, height: 40)
                awayScorersLabel.textColor = UIColor.lightGray
                awayScorersLabel.font = UIFont(name: "Dosis-Regular", size: 25)
                awayScorersLabel.textAlignment = NSTextAlignment.left
                awayScorersLabel.text = item.PlayerWebName + " " + String(item.Minute)  + "'"
                awayScorersLabel.text = awayScorersLabel.text!.uppercased()
                awayScorersStackView.addSubview(awayScorersLabel)
                index = index + 1
            }
        }
        
        
    }

}
