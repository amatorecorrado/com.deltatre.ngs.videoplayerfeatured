//
//  PlayerItem.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


public class PlayerModel {
    public var ID: Int?
    public var BibNumber: String?
    public var OfficialName: String?
    public var OfficialSurname: String?
    public var ShortName: String?
    public var Role: String?
    public var `Type`: String?
    //public var ShortName: String?
    public var ThumbSmall: String?
    public var ThumbMedium: String?
    public var ThumbLarge: String?
    
    //public var PhotoURL: String?
    public var IsTeam: Bool!
    public var IsCaptain: Bool!
    public var IsGoalkeeper: Bool!
    
//    public var IsYellow: Bool!
//    public var IsDoubleYellow: Bool!
//    public var IsRed: Bool!
//    public var IsSubstitutionIN: Bool!
//    public var IsSubstitutionOUT: Bool!
//    public var SubstitutionMinuteIN: Int!
//    public var SubstitutionMinuteOUT: Int!
    
    public var events: [EventModel]?
    
    public init() {
        self.ID = -1
        self.BibNumber = ""
        self.OfficialName = ""
        self.OfficialSurname = ""
        self.ThumbLarge = ""
        self.ThumbMedium = ""
        self.ThumbSmall = ""
        self.IsTeam = false
        self.IsCaptain = false
        self.IsGoalkeeper = false
//        self.IsYellow = false
//        self.IsDoubleYellow = false
//        self.IsRed = false
//        self.IsSubstitutionIN = false
//        self.IsSubstitutionOUT = false
//        self.SubstitutionMinuteIN = 0
//        self.SubstitutionMinuteOUT = 0
        self.events = nil
    }
    
    public func isLineup() -> Bool{
         return (self.Type == "lineup")
    }
    
    public func isBench() -> Bool{
        return (self.Type == "bench")
    }
}
