//
//  LineupModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 04/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


public struct LineupModel{
    public var Homes: [PlayerModel]? = []
    public var Aways: [PlayerModel]? = []
}
