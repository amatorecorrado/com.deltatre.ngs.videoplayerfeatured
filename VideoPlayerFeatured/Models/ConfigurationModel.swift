//
//  Configuration.swift
//  AppleTV
//
//  Created by Corrado Amatore on 14/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
public class ConfigurationModel {
    var id: String?
    var type: String?
    var feeds: [String:String] = [:]
    var images: [String:String] = [:]
    var parameters: [String:String] = [:]
    var actions: [String:String] = [:]
    var labels: [String:String] = [:]
    var data: [String:String] = [:]
    
    public init() {
        
    }
}
extension Dictionary where Key: StringLiteralConvertible, Value: StringLiteralConvertible {
    public func getValue(key: String) -> String?{
        if let dict = (self as? AnyObject) as? Dictionary<String, AnyObject> {
            let val = dict[key] as? String
            if(val != nil){
                return val!
            }
        }
        return ""
    }
}
