//
//  CameraModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 19/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct CameraModel{
    public var ID: String?
    public var Code: String?
    public var Name: String?
    
    public init() {
        ID = nil
        Code = nil
        Name = nil
    }
    
    public init(ID: String, Code: String, Name: String) {
        self.ID = ID
        self.Code = Code
        self.Name = Name
    }

}