//
//  VideoDataModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 01/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct VideoDataModel {
    
    var format = ""
    var eventId = ""
    var videoUrl = ""
    var timeCodeIn = ""
    var trimIn : Double = 0
    var trimOut: Double = 0
    var videoId = ""
    var sources: [String:String] = [:]
}
