//
//  EventModel.swift
//  AppleTV
//
//  Created by Corrado Amatore on 06/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation

public struct EventModel{
    public var id: Int?
    public var eventType: String?
    public var description: String?
    public var attributes: [String]?
    public var minute: Int?
    public var playerFromId: Int?
    public var playerToId: Int?
    public var playerFromName: Int?
    public var playerToName: Int?
    public var timeUTC: NSDate
    public var teamToId: Int?
    public var teamFromId: Int?
    public var teamToName: String?
    public var teamFromName: String?
    public var phaseId: Int?
    
    public init() {
        self.timeUTC = NSDate.init(timeIntervalSince1970: 0)
        
    }
}
