//
//  Utility.swift
//  AppleTVNative
//
//  Created by Federico Bortoluzzi on 27/04/16.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import UIKit
import SystemConfiguration


public class Utility {
    
    static public func newImageFromImage(image:UIImage, scaledToSize:CGSize, withAplha: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(scaledToSize, false, 0.0);
        image.draw(in: CGRect(x:0, y:0,width: scaledToSize.width,height: scaledToSize.height), blendMode: CGBlendMode.normal, alpha: withAplha)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
 
    static public func getMatchIdSplitted(matchId: Int) -> [String] {
        var matchIdArray: [String] = [String]()
        
        var matchIdString = String(matchId)
        if (matchIdString.characters.count > 5) {
            matchIdArray.append((matchIdString as NSString).substring(to: 2))
            matchIdString = String(matchIdString.characters.dropFirst(2))
            matchIdArray.append((matchIdString as NSString).substring(to: 2))
            matchIdString = String(matchIdString.characters.dropFirst(2))
            matchIdArray.append(matchIdString)
        }
        
        return matchIdArray;
    }
    
    static public func deserializeCupInfo(json: Array<Dictionary<String, AnyObject>>, cup: Int, cupSeason: Int, season: Int, roundId: inout Int, matchDay: inout Int, session: inout Int) {
        for item: Dictionary<String, AnyObject> in json {
            if let cupTmp = item["cup"] as? Int, let cupSeasonTmp = item["cupSeason"] as? Int, let seasonTmp = item["season"] as? Int {
                if (cupTmp == cup && cupSeasonTmp == cupSeason && seasonTmp == season) {
                    if let roundIdTmp = item["roundid"] as? Int {
                        roundId = roundIdTmp
                    }
                    if let matchDayTmp = item["matchday"] as? Int {
                        matchDay = matchDayTmp
                    }
                    if let sessionTmp = item["session"] as? Int {
                        session = sessionTmp
                    }
                    break
                }
            }
        }
    }
    
    
    //Image GET from network or memory
    private static var imagesDictionary: Dictionary<String, UIImage>? = [:]
    
    static public func GetCachedImage(key: String, url: String?)->UIImage?{
        if url == nil && key.characters.count == 0 && url?.characters.count == 0{
            return nil
        }
        if(imagesDictionary != nil && imagesDictionary![key] != nil){
            return imagesDictionary![key]
        }else{
            if let url = url{
                let imgUrl = NSURL(string: url)
                if let imgUrl = imgUrl{
                    let imgData = NSData(contentsOf: imgUrl as URL)
                    if let imgData = imgData{
                        let img = UIImage(data: imgData as Data)
                        imagesDictionary![key] = img
                        return img
                    }
                }
            }
        }
        return nil
    }
    
    static public func ResetChacheImage(){
        imagesDictionary = [:]
    }
    
    
    static public func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
        
    }
    
    static func timerStop( timer: inout Timer?){
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
    }
    
    static func setDateLabel(date: NSDate) -> String{
        let userCalendar = Calendar(identifier: .gregorian)
        let dateComponents = Set<Calendar.Component>([.year, .month, .day])
        let datetime = userCalendar.dateComponents(
            dateComponents,
            from: date as Date)
        let month = datetime.month
        
        let df: DateFormatter = DateFormatter()
        let monthName: String = df.monthSymbols[(month! - 1)].uppercased()
        return String(describing: datetime.day) + " " + monthName + " " + String(describing: datetime.year)
    }
    
    static func setDateValue(date: NSDate) -> String{
        let userCalendar = Calendar(identifier: .gregorian)
        let hourMinuteComponents = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second])
        let datetime = userCalendar.dateComponents(hourMinuteComponents, from: date as Date)
        
        return String(describing: datetime.year)+String(format: "%02d", datetime.month!)+String(format: "%02d", datetime.day!)
    }
    
    static func setTime(date: NSDate) -> String{
        let userCalendar = Calendar(identifier: .gregorian)
        let hourMinuteComponents = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second])
        let datetime = userCalendar.dateComponents(hourMinuteComponents, from: date as Date)
        
        let timeStr = String(format: "%02d",datetime.hour!) + String(":") + String(format: "%02d",datetime.minute!)
        
        return String("\(timeStr)").uppercased()
    }
    
    static func ReplaceParameters(text: String, parameters: ParametersModel) -> String{
       var res = text

        if let val = parameters.Cup{
            res = res.replacingOccurrences(of: "{CUP}", with: val)
        }
        if let val = parameters.CupCode{
            res = res.replacingOccurrences(of: "{CUPCODE}", with: val)
        }
        if let val = parameters.CupSeason{
            res = res.replacingOccurrences(of: "{CUPSEASON}", with: val)
        }
        if let val = parameters.Season{
            res = res.replacingOccurrences(of: "{SEASON}", with: val)
        }
        if let val = parameters.Matchday{
            res = res.replacingOccurrences(of: "{MATCHDAY}", with: val)
        }
        if let val = parameters.RoundId{
            res = res.replacingOccurrences(of: "{ROUNDID}", with: val)
        }
        if let val = parameters.MatchId{
            res = res.replacingOccurrences(of: "{MATCHID}", with: val)
        }
        return res
    }
    
    
//    func viewWithIDString(idstring: String, containerView: UIView) -> UIView?{
//        for view in containerView.subviews{
//            if let advancedButton = view as? AdvancedButton{
//                if(advancedButton.IDString == idstring){
//                    return advancedButton
//                }
//            }else if let advancedButton = view as? HomepageButton{
//                if(advancedButton.IDString == idstring){
//                    return advancedButton
//                }
//            }
//        }
//        return nil
//    }
}
