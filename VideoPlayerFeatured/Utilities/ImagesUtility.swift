//
//  File.swift
//  AppleTV
//
//  Created by Corrado Amatore on 16/11/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

//public var _sharedInstance: ImagesUtility?

open class ImagesUtility{
    
    
    var forceDownload: Bool = false
    var useLocalStorage: Bool = true
    
    public static let sharedInstance = ImagesUtility()
    
    public func getImage(urlString: String) -> UIImage?{
        let url = URL(string: urlString)
        
        let fileName: String = url!.lastPathComponent
        var image: UIImage?
        if(useLocalStorage){
            image = getStoredImage(imageName: fileName,url: url!)
        }
        
        if(forceDownload || image == nil){
            image = downloadImage(url: url!)
            if(image != nil && useLocalStorage){
                storeImage(image: image!, imageName: fileName)
            }
        }
        return image
    }
    
    private func storeImage(image: UIImage, imageName: String){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageName)
        print(paths)
        let imageData = UIImagePNGRepresentation(image)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
    }
    
    private func getStoredImage(imageName: String, url: URL) -> UIImage?{
        var image: UIImage? = nil
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePAth){
            image = UIImage(contentsOfFile: imagePAth)
        }
        return image
    }
    
    private func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    public func downloadImage(urlString: String) -> UIImage?{
        let url = URL(string: urlString)
        let image = downloadImage(url:url!)
        return image
    }
    
    private func downloadImage(url: URL) -> UIImage?{
        var image: UIImage?
        let data = try? Data(contentsOf: url)
        if(data != nil){
            image = UIImage(data: data!)
        }
        return image
    }
}
