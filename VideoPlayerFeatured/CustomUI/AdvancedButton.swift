//
//  AdvancedButton.swift
//  AppleTV
//
//  Created by Corrado Amatore on 15/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation


import Foundation
import UIKit


class AdvancedButton: UIButton
{
    var IDString: String = ""
    internal var initialBackgroundColour: UIColor!
    private var group = UIMotionEffectGroup()
    private var _backgroundColorSelected: UIColor! = UIColor.black.withAlphaComponent(0)
    private var _alphaColorSelected: Float! = 0.0
    
    func backgroundColorSelected(SelectedColor: UIColor, Alpha: Float){
        _backgroundColorSelected = SelectedColor
        _alphaColorSelected = Alpha
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        coordinator.addCoordinatedAnimations(
            {
                if self.isFocused
                {
                    let back = self._backgroundColorSelected.withAlphaComponent(CGFloat(self._alphaColorSelected))                    
                    self.backgroundColor = back
                    
                    let amount = 10
                    
                    let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
                    horizontal.minimumRelativeValue = -amount
                    horizontal.maximumRelativeValue = amount
                    
                    let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
                    vertical.minimumRelativeValue = -amount
                    vertical.maximumRelativeValue = amount
                    
                    
                    self.group.motionEffects = [horizontal, vertical]
                    self.addMotionEffect(self.group)
                    //self.layer.cornerRadius = 30
                    
                    self.layer.masksToBounds = false
                    self.layer.shadowColor = UIColor.white.cgColor
                    self.layer.shadowOffset = CGSize(width: 0, height: 20)
                    self.layer.shadowRadius = 30
                    self.layer.shadowOpacity = 0.25
                    self.layer.shadowPath = UIBezierPath( rect: self.layer.bounds).cgPath

                    
                }
                else
                {
                    self.layer.shadowOpacity = 0
                    self.backgroundColor = self.initialBackgroundColour
                    self.removeMotionEffect(self.group)
                }
            },
            completion: nil)
    }

    
}
