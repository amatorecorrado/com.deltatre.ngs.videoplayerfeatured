//
//  AdvancedScrollview.swift
//  AppleTV
//
//  Created by Corrado Amatore on 15/07/2016.
//  Copyright © 2016 Deltatre. All rights reserved.
//

import Foundation
import UIKit

class AdvancedScrollview:UIScrollView{
    
    func viewWithIDString(idstring: String) -> UIView?{
        for view in self.subviews{
            if let advancedButton = view as? AdvancedButton{
                if(advancedButton.IDString == idstring){
                    return advancedButton
                }
            }
            if let customButton = view as? CustomButton{
                if(customButton.tag == Int(idstring)){
                    return customButton
                }
            }
        }
        return nil
    }
}
